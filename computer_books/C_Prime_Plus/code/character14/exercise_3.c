#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char *s_gets(char *st, int n);
#define MAXTITL 40
#define MAXAUTL 40

#define MAXBKS 100
struct book {
  char title[MAXTITL];
  char author[MAXAUTL];
  float value;
};

void sort_book(struct book *, int);

int main(void) {
  struct book library[MAXBKS];
  int count = 0;
  int index;
  printf("Please enter the book title.\n");
  printf("Press [enter] at the start a line to stop.\n");
  while (count < MAXBKS && s_gets(library[count].title, MAXTITL) != NULL &&
         library[count].title[0] != '\0') {
    printf("Now enter the author.\n");
    s_gets(library[count].author, MAXAUTL);
    printf("Now enter the value.\n");
    scanf("%f", &library[count++].value);
    while (getchar() != '\n') {
      continue;
    }
    if (count < MAXBKS) {
      printf("Enter the next title.\n");
    }
  }
  sort_book(library, count);
  if (count > 0) {
    printf("Here is the list of your books:\n");
    for (index = 0; index < count; index++) {
      printf("%s by %s: $%.2f\n", library[index].title, library[index].author,
             library[index].value);
    }
  } else {
    printf("No books?Too bad.\n");
  }
  return 0;
}

char *s_gets(char *st, int n) {
  char *ret_val;
  char *find;
  ret_val = fgets(st, n, stdin);
  if (ret_val) {
    // 查找换行符
    find = strchr(st, '\n');
    // 如果地址不是 NULL,
    if (find)
      *find = '\0';
    // 在此处放置一个空字符
    else
      while (getchar() != '\n')
        continue;
    // 处理输入行中剩余的字符
  }
  return ret_val;
}

void sort_book(struct book *n, int num) {
  int t, i, j;
  for (i = 0; i < num; ++i) {
    for (j = i; j < num; ++j) {
      if ((n + i)->value > (n + j)->value) {
        t = (n + i)->value;
        (n + i)->value = (n + j)->value;
        (n + j)->value = t;
      }
    }
  }
}
