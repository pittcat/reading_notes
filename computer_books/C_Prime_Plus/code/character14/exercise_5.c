#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define C_SIZE 4

struct name {
  char fname[20];
  char lname[20];
};
struct student {
  struct name s_name;
  float grade[3];
  float average;
};

char *s_gets(char *st, int n);
void g_std_info(struct student *, int);
void show_info(struct student *, int);
float average(struct student *, int);
void eatline(void);

int main() {
  struct student stds[C_SIZE];
  g_std_info(stds, C_SIZE);
  show_info(stds, C_SIZE);
  printf("All students average:%f", average(stds, C_SIZE));
  return 0;
}

char *s_gets(char *st, int n) {
  char *ret_val;
  char *find;
  ret_val = fgets(st, n, stdin);
  if (ret_val) {
    // 查找换行符
    find = strchr(st, '\n');
    // 如果地址不是 NULL,
    if (find)
      *find = '\0';
    // 在此处放置一个空字符
    else
      while (getchar() != '\n')
        continue;

    // 处理输入行中剩余的字符
  }
  return ret_val;
}

void g_std_info(struct student *std, int n) {
  float sum;
  for (int i = 0; i < n; ++i) {
    sum = 0.0;
    printf("Please input student first name:");
    s_gets(std[i].s_name.fname, 20);
    printf("Please input student last name:");
    s_gets(std[i].s_name.lname, 20);
    printf("Please input 3 subjects grade:");
    scanf("%f %f %f", &(std[i].grade[0]), &(std[i].grade[1]),
          &(std[i].grade[2]));
    eatline();
    for (int j = 0; j < 3; ++j) {
      sum += std[i].grade[j];
    }
    std[i].average = sum / 3.0;
  }
}

float average(struct student *std, int n) {
  float aver = 0.0;
  for (int i = 0; i < n; ++i) {
    aver = std[i].average;
  }
  return aver / n;
}

void show_info(struct student *std, int n) {
  for (int i = 0; i < n; ++i) {
    printf("Name: %s %s, Grade:%f %f %f,Average:%f\n", std[i].s_name.lname,
           std[i].s_name.fname, std[i].grade[0], std[i].grade[1],
           std[i].grade[2], std[i].average);
  }
}

void eatline(void) {
  while (getchar() != '\n')
    continue;
}
