#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 20
struct month {
  char mon[LEN];
  char bref[4];
  int day;
  int number;
};
struct month months[12] = {
    {"January", "jan", 31, 1},   {"February", "feb", 28, 2},
    {"March", "mar", 31, 3},     {"April", "apr", 30, 4},
    {"May", "may", 31, 5},       {"June", "jun", 30, 6},
    {"July", "jul", 31, 7},      {"August", "aug", 31, 8},
    {"September", "sep", 30, 9}, {"October", "oct", 31, 10},
    {"November", "nov", 30, 11}, {"December", "dec", 31, 12}};
int day(char *bref);
int main() {

  int all_day;
  char mon[4];

  printf("Please input month number:");
  scanf("%s", mon);
  all_day = day(mon);
  printf("all day is %d\n", all_day);

  return 0;
}

int day(char *bref) {
  int sum = 0, i, mon_n;
  for (i = 0; i < 12; ++i) {
    if (strcmp(bref, months[i].bref) == 0) {
      mon_n = i + 1;
      break;
    }
  }
  for (i = 0; i < mon_n; ++i) {
    sum += months[i].day;
  }
  return sum;
}
