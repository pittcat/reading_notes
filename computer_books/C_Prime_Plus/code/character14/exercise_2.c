#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 20
struct month {
  char mon[LEN];
  char bref[4];
  int day;
  int number;
};
struct time {
  int all_day;
  int year;
};
struct month months[12] = {
    {"January", "jan", 31, 1},   {"February", "feb", 28, 2},
    {"March", "mar", 31, 3},     {"April", "apr", 30, 4},
    {"May", "may", 31, 5},       {"June", "jun", 30, 6},
    {"July", "jul", 31, 7},      {"August", "aug", 31, 8},
    {"September", "sep", 30, 9}, {"October", "oct", 31, 10},
    {"November", "nov", 30, 11}, {"December", "dec", 31, 12}};

struct time cal_day(void);
int main(int argc, char *argv[]) {
  struct time t_info;

  t_info = cal_day();
  printf("all day is %d.\n", t_info.all_day);
  return 0;
}

struct time cal_day(void) {
  int day, year, i, mon_n;
  struct time info;
  char mon[15];
  printf("Please input year:");
  while (scanf("%d", &year) != 1) {
    printf("Input info error.\nPlease reinput year.\n");
  }
  printf("Please input month.\n");
  while (scanf("%s", mon) != 1) {
    printf("Input info error.\nPlease reinput month.\n");
  }
  printf("Please input day.\n");

  while (scanf("%d", &day) != 1) {
    printf("Input info error.\nPlease reinput day.\n");
  }
  info.all_day = 0, info.year = year;

  for (i = 0; i < 12; ++i) {
    if (strcmp(mon, months[i].bref) == 0) {
      mon_n = i;
      break;
    }
  }
  for (i = 0; i < mon_n; ++i) {
    info.all_day += months[i].day;
  }
  info.all_day += day;
  return info;
}
