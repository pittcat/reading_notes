#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct names {
  char fname[20];
  char mname[20];
  char lname[20];
};

struct social_insurance_account {
  long int num;
  struct names name;
};

void display1(struct social_insurance_account *, int);
void display2(struct social_insurance_account);

int main() {
  struct social_insurance_account accounts[5] = {
      {302039823, {"Flossie", "March", "Dribble"}}};
  display1(accounts, 5);
  for (int i = 0; i < 5; ++i) {
    if (strlen(accounts[i].name.fname) > 0) {
      display2(accounts[i]);
    }
  }
  return 0;
}

void display1(struct social_insurance_account *account, int n) {
  for (int i = 1; i < n; ++i) {
    if (strlen(account->name.fname) > 0) {
      printf("%s,%s %c.-%lu\n", account->name.lname, account->name.fname,
             account->name.mname[0], account->num);
    }
    account += i;
  }
}

void display2(struct social_insurance_account account) {
  printf("%s,%s %c.-%lu\n", account.name.lname, account.name.fname,
         account.name.mname[0], account.num);
}
