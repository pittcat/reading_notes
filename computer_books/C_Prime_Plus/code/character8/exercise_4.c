#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  int ct = 0;
  char prev = ' ', next = ' ';
  bool state = true;
  while ((next = getchar()) != EOF) {
    if (ispunct(next) != 0) {
      state = false;
    }
    if (isspace(next) && state) {
      ct++;
    }
    while (state != true) {
      next = getchar();
      prev = next;
      if (ispunct(next) && isspace(prev)) {
        state = true;
        break;
      }
    }
  }

  printf("count:%d\n", ct);
  return 0;
}
