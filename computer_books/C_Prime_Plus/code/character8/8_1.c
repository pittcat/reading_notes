#include <stdio.h>
#include <stdlib.h>

int main() {
  char ch;
  while ((ch = getchar()) != '#')
    putchar(ch);
  return 0;
}
