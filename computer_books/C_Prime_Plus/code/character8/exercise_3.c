#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  int lower_n, upper_n;
  lower_n = upper_n = 0;
  char ch;
  while ((ch = getchar()) != EOF) {
    if (islower(ch)) {
      lower_n++;
    } else if (isupper(ch)) {
      upper_n++;
    }
  }
  printf("lower:%d , upper: %d\n", lower_n, upper_n);
  return 0;
}
