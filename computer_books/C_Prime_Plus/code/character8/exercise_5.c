#include <stdio.h>
#include <stdlib.h>

int main() {
  int n, guess = 23;
  printf("Please input a number:");
  scanf("%d", &n);
  while (n != guess) {
    if (n < guess) {
      printf("small\n");
    } else {
      printf("big\n");
    }
    printf("Please input a number:");
    scanf("%d", &n);
  }
  printf("number:%d\n", n);
  return 0;
}
