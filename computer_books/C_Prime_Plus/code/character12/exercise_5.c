#include <stdio.h>
#include <stdlib.h>
#define N 100

void desend_sort(int arr[],int n);

int main() {
    int arr[N];
    for (int i = 0; i < N; ++i) {
      arr[i]=rand()%10;
    }
    /* original array; */
    printf("original array:");
    for (int i = 0; i < N; ++i) {
      printf("%d ",arr[i]);
    }
    desend_sort(arr, N);
    printf("\n");
    /* sorted array; */
    printf("sorted array:\n");
    for (int i = 0; i < N; ++i) {
      printf("%d ",arr[i]);
    }
    return 0;
}


void desend_sort(int arr[],int n){
  int temp;
  for (int i = 0; i < N; ++i) {
    for (int j = i+1; j < N; ++j) {
      if (arr[i]<arr[j]) {
        temp=arr[i];
        arr[i]=arr[j];
        arr[j]=temp;
      }
    }
  }
}
