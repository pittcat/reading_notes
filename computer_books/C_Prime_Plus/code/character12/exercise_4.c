#include <stdio.h>
#include <stdlib.h>
int fun( int n) {
  printf("fun:%d\n",n);
  n++;
  return n;
}

int main() {
    int n=0;
    for (int i = 0; i < 5; ++i) {
      n=fun(i);
    }
    printf("n:%d\n",n);
    return 0;
}
