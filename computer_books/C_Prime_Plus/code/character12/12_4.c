#include <stdio.h>
#include <stdlib.h>

int units = 0;
void critic(void);

int main() {

  extern int units;
  printf("How many pounds to a firkin of butter?\n");
  scanf("%d", &units);
  while (units != 56) {
    critic();
  }
  printf("You must have looed it up!\n");
  return 0;
}

void critic(void) {
  printf("No luck, my friend. Try again.\n");
  scanf("%d", &units);
}
