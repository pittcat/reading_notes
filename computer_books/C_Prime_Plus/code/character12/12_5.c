#include <stdio.h>
#include <stdlib.h>
#include "12_6.h"
int count = 0;

extern void accumulate(int k);
void report_value();

int main() {
  int value;
  register int i;
  printf("Enter a positive integer (0 to quit):");
  while (scanf("%d", &value) == 1 && value > 0) {
    ++count;
    for (i = value; i >= 0; i--) {
      accumulate(i);
    }
    printf("Enter a positive integer (0 to quit:)");
  }
  report_value();
  return 0;
}

void report_value() { printf("Loop executed %d  times\n", count); }
