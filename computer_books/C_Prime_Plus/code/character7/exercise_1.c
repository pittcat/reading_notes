#include <stdio.h>
#include <stdlib.h>

int main() {
  int space_n = 0, line_n = 0, char_n = 0;
  char c;
  printf("Please input chars:\n");
  while ((c = getchar()) != '#') {
    if (c == '\n') {
      line_n++;
    } else if (c == ' ') {
      space_n++;
    } else {
      char_n++;
    }
  }
  printf("lines: %d , spaces: %d , chars: %d\n", line_n, space_n, char_n);
  return 0;
}
