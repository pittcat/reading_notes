#include <stdio.h>
#include <stdlib.h>

int main() {
  int even_n = 0, odd_n = 0, num, e_sum = 0, o_sum = 0;
  printf("Please input number:\n");
  scanf("%d", &num);
  while (num != 0) {
    if (num % 2 == 0) {
      even_n++;
      e_sum += num;
    } else {
      odd_n++;
      o_sum += num;
    }
    printf("Please input number:\n");
    scanf("%d", &num);
  }
  printf("even sum:%d , even num: %d , even average:%f\n", e_sum, even_n,
         (float)e_sum / even_n);
  printf("odd sum:%d , odd num: %d ,odd average:%f\n", o_sum, odd_n,
         (float)o_sum / odd_n);
  return 0;
}
