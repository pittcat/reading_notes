#include <stdio.h>
#include <stdlib.h>
#define SPACE ' '
int main() {
  char ch;
  ch = getchar();
  while ((ch = getchar()) != '\n') {
    if (ch == SPACE) {
      putchar(ch);
    } else {
      putchar(ch + 1);
    }
  }
  putchar(ch);
  return 0;
}
