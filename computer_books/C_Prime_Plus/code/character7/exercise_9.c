#include <stdio.h>
#include <stdlib.h>

int is_prime(int n) 
{ 
        int i; 
        if(n==2) return 1; 
        if(n%2==0) return 0; 
        for(i=3;i*i<=n;i+=2) 
                if(n%i==0) return 0; 
        return 1; 
} 

int main() {
    int num;
    printf("Please input a number:\n");
    scanf("%d",&num);
    printf("Prime:");
    for (int i = 2; i < num+1; ++i) {
      if (is_prime(i)) {
      printf("%d ",i);
      }
    }
    return 0;
}
