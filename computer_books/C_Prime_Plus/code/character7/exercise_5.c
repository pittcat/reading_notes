#include <stdio.h>
#include <stdlib.h>

int main() {
  char c;
  int cout = 0;
  printf("Please input sentence:\n");
  while ((c = getchar()) != '#') {
    switch (c) {
    case '!':
      printf("!!");
      cout++;
      break;
    default:
      printf("%c", c);
    }
  }
  printf("replace times: %d\n", cout);
  return 0;
}
