#include <stdio.h>
#include <stdlib.h>

#define JUST_CHECKING
#define LIMIT 4

int main() {
    int i;
    int total=0;
    for (i = 0; i <=LIMIT; ++i) {
        total+=2*i*i+1;
#ifdef JUST_CHECKING
        printf("i=%d,runing total = %d\n",i,total);
#endif
    }

printf("Grand total = %d \n",total);
    return 0;
}
