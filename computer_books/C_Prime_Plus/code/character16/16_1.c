#include <stdio.h>
#include <stdlib.h>
#define TWO 2
#define OW                                                                     \
  "Consistency is the last refuge of the unimagina \
tive.-Oscar Wilde"

#define FOUR TWO *TWO
#define PX printf("X is %d.\n", x)
#define FMT "X is %d\n"

int main() {
  int x = TWO;
  PX;
  printf(FMT, x);
  printf("%s\n", OW);
  printf("TWO:OW\n");
  printf("%d:%s\n", TWO, OW);
  return 0;
}
