#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define PR(X, ...) printf("Message " #X ":"__VA_ARGS__);
int main() {
  double x = 48;
  double y;
  y = sqrt(x);
  PR(1, "x = %.2f,y=%.4f\n", x, y);

  return 0;
}
