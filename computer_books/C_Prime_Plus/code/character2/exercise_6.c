#include <stdio.h>
#include <stdlib.h>

int main() {
  int toes = 20;
  printf("toes*2:%d", toes * 2);
  printf("toes^3:%d", toes * toes * toes);
  return 0;
}
