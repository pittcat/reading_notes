#include <stdio.h>
#include <stdlib.h>
double power(double a, double b);
int main() { 
  printf("%f",power(1.3,2));
  return 0; }

double power(double a, double b) {
  double pow = 1;
  int i;
  if (b == 0) {
    if (a == 0) {
      printf("0 to the 0 undefined; using 1 as the value\n");
      pow = 1.0;
    }
  } else if (a == 0) {
    pow = 0.0;
  } else if (b > 0) {
    for (int i = 1; i < b + 1; ++i) {
      pow *= a;
    }
  } else {
    pow = 1.0 / power(a, -b);
  }
  return pow;
}
