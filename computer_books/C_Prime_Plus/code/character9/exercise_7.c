#include <stdio.h>
#include <stdlib.h>

int char_place(char c);

int main() {
  printf("%d\n", char_place('c'));
  return 0;
}

int char_place(char c) {
  if (c >= 97 && c <= 122) {
    return c - 'a' + 1;
  } else if (c >= 65 && c <= 90) {
    return c - 'A' + 1;
  } else {
    return -1;
  }
}
