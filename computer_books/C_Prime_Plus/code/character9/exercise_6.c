#include <stdio.h>
#include <stdlib.h>

void switch_n(double* a,double *b,double *c );

int main() {
    double a=4,b=3,c=5;
    switch_n(&a, &b, &c);
    printf("%f %f %f\n",a,b,c);
    return 0;
}

void switch_n(double* a,double *b,double *c ){
  double t;
  if (*a>*b) {
    t=*a;
    *a=*b;
    *b=t;
  }
  if (*b>*c) {
    t=*b;
    *b=*c;
    *c=t;
  }
  if (*a>*c) {
    t=*a;
    *a=*c;
    *c=t;
  }
}
