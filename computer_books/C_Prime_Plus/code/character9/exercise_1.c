#include <stdio.h>
#include <stdlib.h>

double min(double x, double y);

int main() {
  printf("%f\n",min(1, 2));
  return 0; }

double min(double x, double y) { return x < y ? x : y; }
