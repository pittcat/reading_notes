#include <stdio.h>
#include <stdlib.h>

double average(double x, double y);

int main() {
  double a = 1.2, b = 1.3;
  printf("%f\n", average(a, b));
  return 0;
}

double average(double x, double y) {
  double res = 0, t;
  t = 1 / x;
  res += t;
  t = 1 / y;
  res += t;
  return res / 2;
}
