#include <ctype.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 80
char *getword(char *str, int n);

int main() {
  char input[LEN];
  while (getword(input, 5) != NULL) {
    puts(input);
  }
  puts("Done!");
  return 0;
}

char *getword(char *str, int n) {
  int ch, count = 1;
  while ((ch = getchar()) != EOF && isspace(ch)) {
    continue;
  }
  if (ch == EOF) {
    return NULL;
  } else {
    *str++ = ch;
  }
  while ((ch = getchar()) != EOF && !isspace(ch) && count < n) {
    *str++ = ch;
    count++;
  }
  *str = '\0';
  while ((ch = getchar()) != '\n') {
    continue;
  }
  return str;
}
