#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *reverse_str(char *s);

char *s_gets(char *st, int n);

int main() {
  char s[]="asd";
  reverse_str(s);
  printf("%s\n",s);
  return 0;
}

char *s_gets(char *st, int n) {
  char *ret_val;
  char *find;

  ret_val = fgets(st, n, stdin);
  if (ret_val) {
    find = strchr(st, '\n');
    // look for newline
    if (find)
      // if the address is not NULL,
      *find = '\0';
    // place a null character there
    else
      while (getchar() != '\n')
        continue;
  }
  return ret_val;
}

char *reverse_str(char *s) {
  int len = strlen(s), cur;
  char t;
  for (int i = 0; i < len / 2; ++i) {
    /* t = *(s+i); */
    cur = len - i - 1;
    /* *(s+i) = *(s+cur); */
    /* *(s+cur)=t; */
    t=s[i];
    s[i]=s[cur];
    s[cur]=t;
  }

  return s;
}
