#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void remove_space(char *s);

char *s_gets(char *st, int n);
int main() {
  char s[100] = "a a";
  /* s_gets(s, 100); */
  remove_space(s);
  printf("%s\n", s);
  return 0;
}

char *s_gets(char *st, int n) {
  char *ret_val;
  char *find;

  ret_val = fgets(st, n, stdin);
  if (ret_val) {
    find = strchr(st, '\n');
    // look for newline
    if (find)
      // if the address is not NULL,
      *find = '\0';
    // place a null character there
    else
      while (getchar() != '\n')
        continue;
  }
  return ret_val;
}
void remove_space(char *s) {
  int len = strlen(s), cur = 0;
  char t[len];
  for (int i = 0; i < len; ++i) {
    if (!isspace(s[i])) {
      t[cur] = s[i];
      cur++;
    }
  }
  cur = 0;
  while (t[cur++] != '\0') {
    s[cur] = t[cur];
  }
  s[cur] = '\0';
}
