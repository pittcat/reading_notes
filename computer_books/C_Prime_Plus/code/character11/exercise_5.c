#include <stdio.h>
#include <stdlib.h>

_Bool check_c(char c, char *s);

int main() {

  printf("%d\n", check_c('c', "adc"));
  return 0;
}

_Bool check_c(char c, char *s) {
  _Bool flag = 0;
  while (*s++ != '\0') {
    if (c == *s) {
      flag = 1;
      break;
    }
  }
  return flag;
}
