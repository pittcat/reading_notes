#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEN 90
char *string_in(char *, char *);

char *s_gets(char *st, int n);

int main() {
  char str1[LEN], str2[LEN];
  char *p;
  printf("Please input string1:");
  s_gets(str1, LEN);
  printf("Please input string2:");
  s_gets(str2, LEN);
  while (str1[0] != '\0' && str2[0] != '\0') {
    if ((p = string_in(str1, str2))) {
      printf("%p\n", p);
    }
    printf("Please input string1:");
    s_gets(str1, LEN);
    printf("Please input string2:");
    s_gets(str2, LEN);
  }
  puts("Done!");
  return 0;
}

char *string_in(char *s1, char *s2) {
  int l = strlen(s2);
  char *p;
  for (int i = 0; i < l; ++i) {
    p = strchr(s1, s2[i]);
    if (p) {
      return p;
    }
  }
  return NULL;
}

char *s_gets(char *st, int n) {
  char *ret_val;
  char *find;

  ret_val = fgets(st, n, stdin);
  if (ret_val) {
    find = strchr(st, '\n');
    // look for newline
    if (find)
      // if the address is not NULL,
      *find = '\0';
    // place a null character there
    else
      while (getchar() != '\n')
        continue;
  }
  return ret_val;
}
