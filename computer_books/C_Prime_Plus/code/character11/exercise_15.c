#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int myatoi(char *s);

int main() {
  int n;
  char *s = "123";
  n = myatoi(s);
  printf("%d\n", n);
  return 0;
}

int myatoi(char *s) {
  int res = 0, l, cur;
  l = strlen(s);
  while (*s != '\0') {
    if (!isdigit(*s)) {
      return 0;
    } else {
      cur = *s - '0';
      res += (int)pow(10, l - 1)*cur;
      l--;
    }
    s++;
  }
  return res;
}
