#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 3
#define LEN 100
char menu(void);
char *s_gets(char *st, int n);
void show_string_array(int n, char s[n][LEN]);
void show_ascii_array(int n, char s[n][LEN]);
void show_length_array(int n, char s[n][LEN]);
void show_first_array(int n, char s[n][LEN]);
int check_first_len(char *s);
int main() {
  char string[N][LEN];
  char item;
  for (int i = 0; i < N; ++i) {
    printf("Enter a string:");
    s_gets(string[i], LEN);
  }
  item = menu();
  switch (item) {
  case 'a':
    show_string_array(N, string);
    break;
  case 'b':
    show_ascii_array(N, string);
    break;
  case 'c':
    show_length_array(N, string);
    break;
  case 'd':
    show_first_array(N, string);
    break;
  case 'q':
    printf("Done!\n");
    break;
  }
  return 0;
}

char menu(void) {
  char item;
  printf("%-50s\n", "a. Print string array.");
  printf("%-50s\n", "b.Print string array by ascii.");
  printf("%-50s\n", "c. Print string array by string length.");
  printf("%-50s\n", "d. Print string array by length of first word.");
  printf("e. Quit.\n");
  printf("Please choose function:\n");
  scanf("%c", &item);
  return item;
}

char *s_gets(char *st, int n) {
  char *ret_val;
  char *find;

  ret_val = fgets(st, n, stdin);
  if (ret_val) {
    find = strchr(st, '\n');
    // look for newline
    if (find)
      // if the address is not NULL,
      *find = '\0';
    // place a null character there
    else
      while (getchar() != '\n')
        continue;
  }
  return ret_val;
}

void show_string_array(int n, char s[n][LEN]) {
  for (int i = 0; i < n; ++i) {
    printf("%s\n", s[i]);
  }
}

void show_ascii_array(int n, char s[n][LEN]) {
  char t[n][LEN];
  char temp[LEN];
  int i, j;
  for (i = 0; i < n; ++i) {
    strcpy(t[i], s[i]);
  }
  for (i = 0; i < n; ++i) {
    for (j = i + 1; j < n; ++j) {
      if (strcmp(t[i], t[j]) > 0) {
        strcpy(temp, t[i]);
        strcpy(t[i], t[j]);
        strcpy(t[j], temp);
      }
    }
  }
  for (i = 0; i < n; ++i) {
    printf("%s\n", t[i]);
  }
}

void show_length_array(int n, char s[n][LEN]) {
  char temp[LEN];
  for (int i = 0; i < n; ++i) {
    for (int j = i + 1; j < n; ++j) {
      if (strlen(s[i]) > strlen(s[j])) {
        memcpy(temp, s[i], LEN);
        memcpy(s[i], s[j], LEN);
        memcpy(s[j], temp, LEN);
      }
    }
  }
  for (int i = 0; i < n; ++i) {
    printf("%s\n", s[i]);
  }
}

void show_first_array(int n, char s[n][LEN]) {
  char temp[LEN];
  for (int i = 0; i < n; ++i) {
    for (int j = i + 1; j < n; ++j) {
      if (check_first_len(s[i]) > check_first_len(s[j])) {
        memcpy(temp, s[i], LEN);
        memcpy(s[i], s[j], LEN);
        memcpy(s[j], temp, LEN);
      }
    }
  }

  for (int i = 0; i < n; ++i) {
    printf("%s\n", s[i]);
  }
}

int check_first_len(char *s) {
  int len = 0;
  while (*s != '\0' && *s != ' ') {
    len++;
    s++;
  }
  return len;
}
