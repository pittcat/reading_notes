#include <stdio.h>
#include <stdlib.h>

void ReverseArray(char *arr[], int n) {
  char *temp;

  // Move from begin and end. Keep
  // swapping strings.
  int j = n - 1;
  for (int i = 0; i < j; i++) {
    temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
    j--;
  }
}

int main(int argc, char *argv[]) {
  char *s;
  printf("%d\n", argc);
  for (int i = 0; i < (argc - 1) / 2; ++i) {
    ReverseArray(argv, argc);
  }
  for (int i = 0; i < argc; ++i) {
    printf("%s ", argv[i]);
  }
  return 0;
}
