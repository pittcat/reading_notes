#include <endian.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEN 80
char *s_gets(char *st, int n);
_Bool is_within(char c,const char *s);

int main() {
  char input[LEN];
  char ch;
  char found;
  printf("Enter a string:");
  while (s_gets(input, LEN) && input[0] != '\0') {
    printf("Enter a character:");
    ch = getchar();
    while (getchar() != '\n') {
      continue;
    }
    found = is_within(ch, input);
    if (found == 0) {
      printf("%c not found in string.\n", ch);
    } else {
      printf("%c found in string %s.\n", ch, input);
    }
  }
  puts("Done!");
  return 0;
}

_Bool is_within(char c,const char *s) {

  _Bool flag = 0;
  while (*s++ != '\0') {
    if (c == *s) {
      flag = 1;
      break;
    }
  }
  return flag;
}

char *s_gets(char *st, int n) {
  char *ret_val;
  char *find;

  ret_val = fgets(st, n, stdin);
  if (ret_val) {
    find = strchr(st, '\n');
    // look for newline
    if (find)
      // if the address is not NULL,
      *find = '\0';
    // place a null character there
    else
      while (getchar() != '\n')
        continue;
  }
  return ret_val;
}
