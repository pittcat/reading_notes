#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  int word_n, upper_n, lower_n, punc_n, digits;
  char c;
  bool inword = false;
  word_n = upper_n = lower_n = punc_n = digits = 0;
  printf("Please input a string:");
  while ((c = getchar()) != EOF) {
    if (isupper(c)) {
      upper_n++;
    } else if (islower(c)) {
      lower_n++;
    } else if (ispunct(c)) {
      punc_n++;
    } else if (isdigit(c)) {
      digits++;
    }
    if (!isspace(c) && !inword) {

      inword = true;
      word_n++;
    }
    if (isspace(c) && inword) {
      inword = false;
    }
  }
  printf("words:%d , lower:%d ,upper: %d , digits: %d , puntuation: %d\n",
         word_n, lower_n, upper_n, digits, punc_n);
  return 0;
}
