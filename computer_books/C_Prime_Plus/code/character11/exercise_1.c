#include <endian.h>
#include <stdio.h>
#include <stdlib.h>
#define LEN 10

char *getnchar(char *str, int n);

int main() {
  char input[LEN];
  char *check;
  check=getnchar(input, LEN);
  printf("%s\n",check);
  return 0;
}

char *getnchar(char *str, int n) {
  int i;
  for ( i = 0; i < n; ++i) {
    if ((str[i] = getchar()) == EOF) {
      break;
    }
  }
  str[i]='\0';
  return str;
}
