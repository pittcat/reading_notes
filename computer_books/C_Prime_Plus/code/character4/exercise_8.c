#include <stdio.h>
#include <stdlib.h>

#define GTL 3.785
#define MTK 1.609

int main() {
  float m, c, s;
  printf("Please input mileage and fuel consumption:");
  scanf("%f %f", &m, &c);
  printf("Every mileage consume %.1f gallon\n", c / m);
  printf("Every km consume %.1f litre\n", c * GTL / m * MTK);
  return 0;
}
