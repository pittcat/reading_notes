#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DENSITY 62.4
int main() {
  float weight, volume;
  int size, letter;
  char name[40];
  printf("Hi!What's your first name?\n");
  scanf("%s", name);
  printf("%s,what's your weight in pounds?\n", name);
  scanf("%f", &weight);
  size = sizeof(name);
  letter = strlen(name);
  volume = weight / DENSITY;
  printf("Well,%s,your volume is %2.2f\n", name, volume);
  printf("Also,your first name has %d letters,\n", letter);
  printf("and we have %d bytes to store it.\n", size);
  return 0;
}
