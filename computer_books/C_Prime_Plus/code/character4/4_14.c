#include <stdio.h>
#include <stdlib.h>

int main() {
  /* 1.  */
printf("Here's one way to print a ");
printf("long string.\n");
/* 2.  */
printf("Here's another way to print a\
long string.\n");
printf("Here's another way to print a "
"long string.");
    return 0;
}
