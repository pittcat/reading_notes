#include <stdio.h>
#include <stdlib.h>

int main() {
  double height, inch;
  printf("Please input your height by centimiter:\n");
  scanf("%lf", &height);
  while (height > 0) {
    inch = height * 0.39370;
    printf("%lf cm = %lf\n", height, inch);
    printf("Please input your height by centimiter:\n");
    scanf("%lf", &height);
  }
  return 0;
}
