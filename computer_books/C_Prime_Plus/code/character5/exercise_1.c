#include <stdio.h>
#include <stdlib.h>

int main() {
  const int m_t_h = 60;
  int min, hour;
  printf("Please input time(min):\n");
  scanf("%d", &min);
  while (min > 0) {
    hour = min / m_t_h;
    min %= 60;
    printf("hour:%d,min:%d\n", hour, min);
    printf("Please input time(min):\n");
    scanf("%d", &min);
  }
  return 0;
}
