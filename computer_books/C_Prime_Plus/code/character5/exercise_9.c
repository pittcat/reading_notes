#include <stdio.h>
#include <stdlib.h>

int Temperatures(double Fahrenheit);

int main() {
  double Fahrenheit;
  printf("Please input Fahrenheit:\n");
  scanf("%lf", &Fahrenheit);
  Temperatures(Fahrenheit);
  return 0;
}

int Temperatures(double Fahrenheit) {
  double Celsius = 5.0 / 9.0 * (Fahrenheit - 32.0);
  double Kelvin = Celsius + 273.16;
  printf("Fahrenheit:%.2f\n", Fahrenheit);
  printf("Celsius:%.2f\n", Celsius);
  printf("Kelvin:%.2f\n", Kelvin);
  return 0;
}
