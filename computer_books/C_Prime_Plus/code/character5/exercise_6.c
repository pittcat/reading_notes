#include <stdio.h>
#include <stdlib.h>

int main() {
  int count, sum, day;
  count = 0;
  sum = 0;
  scanf("%d", &day);
  while (count++ < day) {
    sum += count * count;
  }
  printf("sum=%d\n", sum);
  return 0;
}
