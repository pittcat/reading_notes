#include <stdio.h>
#include <stdlib.h>

int main() {
  const int m_t_h = 7;
  int day, week;
  printf("Please input days:\n");
  scanf("%d", &day);
  while (day > 0) {
    week = day / m_t_h;
    day %= 7;
    printf("%d days are %d week,%d day\n",day, week, day);
    printf("Please input time(day):\n");
    scanf("%d", &day);
  }
  return 0;
}
