#include <stdio.h>
#include <stdlib.h>

int main() {
  int num;
  printf("Please input a number:\n");
  scanf("%d", &num);
  for (int i = 0; i < 11; ++i) {
    printf("%d ", num + i);
  }
  return 0;
}
