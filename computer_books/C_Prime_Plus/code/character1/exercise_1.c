#include <stdio.h>
#include <stdlib.h>

int main() {
  double inch;
  double cm;
  scanf("%lf", &inch);
  cm = 2.54 * inch;
  printf("%lf equals %lf centimeter", inch, cm);
  return 0;
}

