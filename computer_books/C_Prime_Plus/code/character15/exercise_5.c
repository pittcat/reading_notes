#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

char *itobs(int n, char *ps);
unsigned int rotate(unsigned int, unsigned int);

int main() {
  unsigned int val;
  unsigned int rot;
  unsigned int place;
  char bstr1[CHAR_BIT * sizeof(int) + 1];

  char bstr2[CHAR_BIT * sizeof(int) + 1];
  printf("Enter the number of bits to be rotated:\n");
  while (scanf("%ud", &val)) {
    printf("Enter the number of bits to rotated:\n");
    if (scanf("%ul", &place) == 1) {
      break;
    }
    rot = rotate(val, place);
    itobs(val, bstr1);
    itobs(val, bstr2);
    printf("%u rotated is %u.\n", val, rot);
    printf("%s rotated is %s.\n", bstr1, bstr2);
    printf("Next value:");
  }
  puts("Done!");
  return 0;
}

char *itobs(int n, char *ps) {
  int i;
  const static int size = CHAR_BIT * sizeof(int);
  for (i = size - 1; i >= 0; i--, n >>= 1) {
    ps[i] = (01 & n) + '0';
  }
  ps[size] = '\0';
  return ps;
}

unsigned int rotate(unsigned int n, unsigned int b) {
  static const int size = CHAR_BIT * sizeof(int);
  unsigned int overflow;
  b %= size;
  overflow = n >> (size - b);
  return (n << b) | overflow;
}
