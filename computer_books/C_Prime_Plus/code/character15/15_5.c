#include <stdio.h>
#include <stdlib.h>

int main() {
  double dx;
  char ca;
  char cx;
  double dz;
  char cb;
  char _Alignas(double) cz;
  printf("char alignment : %zd\n", _Alignof(char));
  printf("double alignment : %zd\n", _Alignof(double));
  printf("&dx:%p\n", &dx);
  printf("&ca:%p\n", &ca);
  printf("&cx:%p\n", &cx);
  printf("&dz:%p\n", &dz);
  printf("&cz:%p\n", &cz);
  return 0;
}
