#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>

int bin2int(const char *);


int main() {
    printf("%d\n",bin2int("10"));
    return 0;
}


int bin2int(const char *s){
    int val=0;
    while (*s!='\0') {
        val=2*val+(*s++-'0');
    }
    return val;

}
