#include <stdio.h>
#include <stdlib.h>

int main() {
  double daphnem = 100, deirdre = 100;
  int year = 0;
  while (deirdre >= daphnem) {
    deirdre += 10;
    daphnem += daphnem * 0.05;
    year++;
  }
  printf("Cost year:%d\n", year);
  printf("Daphnem:%lf\n", daphnem);
  printf("Deirdre:%lf\n", deirdre);
  return 0;
}
