#include <stdio.h>
#include <stdlib.h>
#define SIZE 8

int main() {
  int arr[SIZE];
  printf("Please input 8 numbers:");
  for (int i = 0; i < SIZE; ++i) {
    scanf("%d", &arr[i]);
  }
  printf("reversed numbers:");
  for (int i = SIZE - 1; i >= 0; --i) {
    printf("%d ", arr[i]);
  }
  return 0;
}
