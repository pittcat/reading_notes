#include <stdio.h>
#include <stdlib.h>

int main() {
  int time;
  double cur = 1.0, next = 2.0, sum;
  printf("Please input time:");
  scanf("%d", &time);
  while (time > 0) {
    sum = 1.0;
    for (int i = 0; i < time; ++i) {
      sum += cur / next;
      next += 1;
    }
    printf("1.0 + 1.0/2.0 + 1.0/3.0 + ... = %lf\n", sum);
    sum = 1.0;
    for (int i = 0; i < time; ++i) {
      cur *= -1;
      sum += cur / next;
      next += 1;
    }
    printf("1.0 - 1.0/2.0 - 1.0/3.0 - ... = %lf\n", sum);
    printf("Please input time:");
    scanf("%d", &time);
  }
  return 0;
}
