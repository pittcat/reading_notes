#include <stdio.h>
#include <stdlib.h>
double calculate(double n1, double n2);
int main() {
  double n1, n2;
  printf("Please input two numbers:");
  scanf("%lf%lf", &n1, &n2);
  calculate(n1, n2);
  return 0;
}

double calculate(double n1, double n2) {
  double res;
  res = (n1 - n2) / (n1 * n2);
  return res;
}
