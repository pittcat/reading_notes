#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define SIZE 8
int main() {
  int arr[SIZE], cur = 0;
  printf("Init numbers\n");
  for (int i = 0; i < SIZE; ++i) {
    arr[i] = (int)pow(2, i + 1);
  }
  printf("Print numbers:\n");
  do {
    printf("%d\n", arr[cur]);
  } while (++cur < SIZE);
  return 0;
}
