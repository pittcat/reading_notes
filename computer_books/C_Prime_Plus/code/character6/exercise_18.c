#include <stdio.h>
#include <stdlib.h>

int main() {
  int week = 0, friends = 5;
  while (friends < 150) {
    printf("week %d friends number: %d\n", week, friends);
    week++;
    friends = 2 * (friends - week);
  }
  return 0;
}
