#include <stdio.h>
#include <stdlib.h>

int main() {
  long num;
  long sum = 0L;
  int status;
  printf("Please enter an integer to be summed\n");
  printf("(q to quit): \n");
  status = scanf("%ld", &num);
  printf("%d\n", status);
  /* while (status == 1) { */
  while (scanf("%ld", &num)==1) {
    sum += num;
    printf("Please enter next integer (q to quit):");
    status = scanf("%ld", &num);
  }
  printf("Those integers sum to %ld.\n", sum);
  return 0;
}
