#include <stdio.h>
#include <stdlib.h>

int main() {
  int lower, upper, index;
  int square, cube;
  printf("Enter starting integer:");
  scanf("%d", &lower);
  printf("Enter ending integer:");
  scanf("%d", &upper);
  printf("%5s %10s %15s\n", "num", "square", "cube");
  for (index = 0; index <= upper; ++index) {
    square = index * index;
    cube = square * index;
    printf("%5d %10d %15d\n", index, square, cube);
  }
  return 0;
}
