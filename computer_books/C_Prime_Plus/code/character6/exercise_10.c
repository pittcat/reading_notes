#include <stdio.h>
#include <stdlib.h>

int calculate(int n1, int n2);

int main() {
  int lower, upper, sum = 0;
  printf("Enter lower and upper integer limits:");
  scanf("%d %d", &lower, &upper);
  while (lower < upper) {
    sum = calculate(lower, upper);
    printf("The sums of the squares from %d to %d is %d\n", lower, upper, sum);
    printf("Enter next set of limits:");
    scanf("%d %d", &lower, &upper);
  }
  printf("Done\n");
  return 0;
}

int calculate(int n1, int n2) {
  int sum = 0;
  for (int i = n1; i < n2 + 1; ++i) {
    sum += i * i;
  }
  return sum;
}
