#include <stdio.h>
#include <stdlib.h>
#define SIZE 8
int main() {
    double arr1[SIZE],arr2[SIZE],cur_sum=0.0;
    printf("Init array:\n");
    for (int i = 0; i < SIZE; ++i) {
      printf("Please input number:");
      scanf("%lf",&arr1[i]);
      cur_sum+=arr1[i];
      arr2[i]=cur_sum;
    }
    printf("array1:");
    for (int i = 0; i < SIZE; ++i) {
      printf("%5.2lf ",arr1[i]);
    }
    printf("\n");
    
    printf("array2:");
    for (int i = 0; i < SIZE; ++i) {
      printf("%5.2lf ",arr2[i]);
    }
    return 0;
}
