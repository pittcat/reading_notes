#include <stdio.h>
#include <stdlib.h>
#define SIZE 26
int main() {
  char lcase[SIZE];
  for (int i = 0; i < SIZE; ++i) {
    lcase[i] = 'a' + i;
  }
  for (int i = 0; i < SIZE; ++i) {
    printf("%c", lcase[i]);
  }
  printf("\n");
  return 0;
}
