#include <stdio.h>
#include <stdlib.h>

int main() {
  char init = 'A', final, cur;
  scanf("%c",&final);
  int disdance = final - init + 1;
  for (int i = 1; i < disdance + 1; ++i) {
    for (int j = 0; j < i; ++j) {
      printf("%c", init + j);
    }
    cur = init + i - 1;
    for (int j = 1; j < i; ++j) {
      printf("%c", cur - j);
    }
    printf("\n");
  }
  return 0;
}
