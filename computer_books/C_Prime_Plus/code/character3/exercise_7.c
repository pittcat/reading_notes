#include <stdio.h>
#include <stdlib.h>

int main() {
  float height;
  printf("Please input height(inch):\n");
  scanf("%f", &height);
  printf("The height is %f cm\n", height * 2.54);
  return 0;
}
