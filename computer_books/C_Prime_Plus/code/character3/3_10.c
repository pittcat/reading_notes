#include <stdio.h>
#include <stdlib.h>

int main() {
  float salary;
  printf("\aEnter your desired monthly salary:");
  printf("$______\b\b\b\b\b");
  scanf("%f", &salary);
  /* salary=12.3; */
  printf("\n\t$%.2f a month is $%.2f a year.", salary, salary * 12.0);
  printf("Gee!\n");
  return 0;
}
