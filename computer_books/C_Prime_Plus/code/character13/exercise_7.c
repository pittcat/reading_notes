#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  FILE *t1, *t2;
  char c1, c2;
  int line = 1;
  if (argc != 3) {
    printf("Usage: %s file file\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  if ((t1 = fopen(argv[1], "r")) == NULL) {
    printf("Could not open file1\n");
    exit(EXIT_FAILURE);
  }
  if ((t2 = fopen(argv[2], "r")) == NULL) {
    printf("Could not open file2\n");
    exit(EXIT_FAILURE);
  }
  while ((c1 = getc(t1)) != EOF || (c2 = getc(t2)) != EOF) {
    printf("line %d:", line);
    printf("file1:");
    while (c1 != '\n' && c1 != EOF) {
      putc(c1, stdout);
      c1 = getc(t1);
    }
    printf(" file2:");
    while (c2 != '\n' && c2 != EOF) {
      putc(c2, stdout);
      c2 = getc(t2);
    }
    printf("\n");
    line++;
  }
  fclose(t1);
  fclose(t2);
  return 0;
}
