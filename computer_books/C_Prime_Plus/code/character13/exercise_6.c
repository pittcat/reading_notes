#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEN 40
int main() {

  FILE *in, *out;
  int ch;
  char name[LEN];
  char init_name[LEN];
  int count = 0;

  printf("Please input file name:");
  scanf("%s", init_name);
  if ((in = fopen(init_name, "r")) == NULL) {
    fprintf(stderr, "I couldn't open the file \"%s\"\n", init_name);
    exit(EXIT_FAILURE);
  }
  strncpy(name, init_name, LEN - 5);
  name[LEN - 5] = '\0';
  strcat(name, ".red");
  if ((out = fopen(name, "w")) == NULL) {
    fprintf(stderr, "Can't creat output file.\n");
    exit(3);
  }
  while ((ch = getc(in)) != EOF) {
    if (count++ % 3 == 0) {
      putc(ch, out);
    }
  }
  if (fclose(in) != 0 || fclose(out) != 0) {
    fprintf(stderr, "Error in closing files.\n");
  }
  return 0;
}
