#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  char buf[10];
  char ch;
  FILE *fp;
  char filename[20];
  unsigned long count = 0;
  printf("Please input filename:");
  scanf("%s", filename);
  fp = fopen(filename, "w+");
  while (fgets(buf, sizeof buf, stdin) != NULL)
    fputs(buf, fp);

  fclose(fp);
  fp = fopen(filename, "r");
  while ((ch=getc(fp))!=EOF) {
    putc(ch, stdout);
    count++;
  }

  fclose(fp);
  printf("File %s has %lu characters\n", filename, count);
  return 0;
}
