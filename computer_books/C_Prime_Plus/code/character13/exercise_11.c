#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEN 90

int main(int argc, char *argv[]) {
  char *cur, pair_str[LEN];
  int cur_l;
  if (argc != 3) {
    printf("Usage: %s string file\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  cur = argv[1];
  FILE *f;
  if ((f = fopen(argv[2], "r")) == NULL) {
    printf("Could not open file\n");
    exit(EXIT_FAILURE);
  }
  while ((fgets(pair_str, LEN, f)) != NULL) {
    if (strstr(pair_str, cur) != NULL) {
      printf("%s", pair_str);
    }
  }
  fclose(f);

  return 0;
}
