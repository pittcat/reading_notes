#include <ctype.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEN 40
int main() {
  char f_name[LEN];
  char cur;
  FILE *f1;
  int line, count = 0;
  printf("Please input file name:");
  scanf("%s", f_name);
  if ((f1 = fopen(f_name, "r")) == NULL) {
    printf("Could open file %s\n", f_name);
    exit(EXIT_FAILURE);
  }
  printf("Please input line number:");
  while (scanf("%d", &line) == 1 && line != -1) {
    while ((cur = getc(f1)) != EOF && line - 1 != count) {
      if (cur == '\n') {
        count++;
      }
    }
    while ((cur = getc(f1)) != '\n') {
      printf("%c", cur);
    }
    rewind(f1);
    count = 0;
    printf("\n");
    printf("Please input line number:");
  }
  printf("Done!\n");
  fclose(f1);
  return 0;
}
