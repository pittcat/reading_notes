#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  FILE *target;
  char c;
  int count = 0;
  if (argc != 3 && argc != 2) {
    printf("Usage: %s char file or %s file\n", argv[0], argv[0]);
    exit(EXIT_FAILURE);
  }
  if (argc == 2) {
    while ((c = getchar()) != EOF) {
      printf("%c", c);
    }
    exit(EXIT_FAILURE);
  }
  target = fopen(argv[2], "r");
  if (argc == 3) {
    while ((c = getc(target)) != EOF) {
      if (c == argv[1][0]) {
        count++;
      }
    }
  }
  fclose(target);
  printf("%c count is %d\n", argv[1][0], count);
  return 0;
}
