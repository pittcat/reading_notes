#include <ctype.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  FILE *source;
  FILE *target;
  FILE *tmp = tmpfile();
  char cur_c;
  if (argc != 2) {
    printf("Usage: %s filename\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  if ((source = fopen(argv[1], "r")) == NULL) {
    printf("Could not open source:%s\n", argv[1]);
    exit(EXIT_FAILURE);
  }
  if (tmp) {
    while ((cur_c = getc(source)) != EOF) {
      putc(cur_c, tmp);
    }
  }

  if ((target = fopen(argv[1], "w")) == NULL) {
    printf("Could not open target:%s\n", argv[1]);
    exit(EXIT_FAILURE);
  }
  rewind(tmp);
  while ((cur_c = getc(tmp)) != EOF) {
    if (islower(cur_c)) {
      cur_c = toupper(cur_c);
    }
    /* printf("%c\n", cur_c); */
    putc(cur_c, target);
  }

  if (fclose(target)) {
    printf("Could close target file\n");
    exit(EXIT_FAILURE);
  }
  if (fclose(source)) {
    printf("Could close source file\n");
    exit(EXIT_FAILURE);
  }
  return 0;
}
