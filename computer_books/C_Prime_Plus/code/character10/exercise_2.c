#include <stdio.h>
#include <stdlib.h>

void copy_arr1(double t[], double s[], int n);
void copy_arr2(double *t, double *s, int n);
void copy_arr3(double *t, double *start, double *end);

int main() {
  double source[5] = {1.1, 2.2, 3.3, 4.4, 5.5};
  double target1[5], target2[5], target3[5];

  copy_arr1(target1, source, 5);
  copy_arr2(target2, source, 5);
  copy_arr3(target3, source, source + 5);
  return 0;
}

void copy_arr1(double t[], double s[], int n) {
  for (int i = 0; i < n; ++i) {
    t[i] = s[i];
  }
}

void copy_arr2(double *t, double *s, int n) {
  for (int i = 0; i < 5; ++i) {
    *(t + i) = s[i];
  }
}

void copy_arr3(double *s, double *start, double *end) {
  int i = 0;
  while (start < end) {
    *(start + i) = s[i];
    i++;
  }
}
