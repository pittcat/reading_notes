#include <stdio.h>
#include <stdlib.h>
int find_max(int arr[], int n);

int main() {
  int arr[] = {2, 3, 45, 6, 90};
  printf("%d\n", find_max(arr, sizeof(arr) / sizeof(arr[0])));
  return 0;
}

int find_max(int arr[], int n) {
  int max = arr[0];
  for (int i = 0; i < n; ++i) {
    if (arr[i] > max) {
      max = arr[i];
    }
  }
  return max;
}
