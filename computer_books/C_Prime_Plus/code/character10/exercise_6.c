#include <stdio.h>
#include <stdlib.h>

void reverse(double *arr, double *res, int n);

int main() {
  double array[3] = {1, 2, 3};
  double res[3];
  reverse(array, res, sizeof(array) / sizeof(array[0]));
  for (int i = 0; i < 3; ++i) {
    printf("%f\n", res[i]);
  }
  return 0;
}

void reverse(double *arr, double *res, int n) {
  for (int i = 0; i < n; ++i) {
    res[i] = arr[n - i - 1];
  }
}
