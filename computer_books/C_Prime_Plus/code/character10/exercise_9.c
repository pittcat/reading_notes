#include <stdio.h>
#include <stdlib.h>

void copy_arr(int n1,int n2,double s[n1][n2],double t[n1][n2]);
void show_array(int n1,int n2,double arr[n1][n2]);
int main() {
    double array[3][3]={1.2,1.4,1.6,1.7,1.0,1.4,1.5};
    double target[3][3];
    copy_arr(3, 3, array, target);
    show_array(3, 3, array);
    return 0;
}


void copy_arr(int n1,int n2,double s[n1][n2],double t[n1][n2]){
  for (int i = 0; i < n1; ++i) {
    for (int j = 0; j < n2; ++j) {
      *( *(t+i)+j )=*( *(s+i)+j );
    }
  } 
}



void show_array(int n1,int n2,double arr[n1][n2]){
  printf("array:");
  for (int i = 0; i < n1; ++i) {
    for (int j = 0; j < n2; ++j) {
      printf("%.2f ",arr[i][j]);
    }
  }
}
