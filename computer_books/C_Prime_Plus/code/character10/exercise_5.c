#include <stdio.h>
#include <stdlib.h>
double difference(double *arr, int n);
int main() {
  double array[] = {1.2, 2.3, 0.1};
  double diff = difference(array, sizeof(array) / sizeof(array[0]));
  printf("diff:%f\n", diff);
  return 0;
}

double difference(double *arr, int n) {
  double max, min, diff;
  max = min = arr[0];
  for (int i = 0; i < n; ++i) {
    if (arr[i] > max) {
      max = arr[i];
    } else if (arr[i] < min) {
      min = arr[i];
    }
  }
  diff = max - min;
  return diff;
}
