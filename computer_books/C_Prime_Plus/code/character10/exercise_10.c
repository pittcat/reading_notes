#include <stdio.h>
#include <stdlib.h>

void add_arr(int *arr1, int *arr2, int *res, int n);

int main() {
  int arr1[] = {2, 4, 5, 8};
  int arr2[] = {1, 0, 4, 6};
  int res[4];
  add_arr(arr1, arr2, res, 4);
  printf("Array:");
  for (int i = 0; i < 4; ++i) {
    printf("%d ", res[i]);
  }
  return 0;
}

void add_arr(int *arr1, int *arr2, int *res, int n) {
  for (int i = 0; i < n; ++i) {
    *(res + i) = *(arr1 + i) + *(arr2 + i);
  }
}
