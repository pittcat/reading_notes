#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Dijkstra algorithm
graphdict={"A":[("B",6),("C",3)], "B":[("C",2),("D",5)],"C":[("B",2),("D",3),("E",4)],\
         "D":[("B",5),("C",3),("E",2),("F",3)],"E":[("C",4),("D",2),("F",5)],"F":[("D",3),"(E",5)]})
assert: start node must be zero in-degree
"""


def Dijkstra(startNode, graphdict=None):
    S = [startNode]
    V = []
    for node in graphdict.keys():
        if node != startNode:
            V.append(node)
    dist = {}
    for node in V:
        dist[node] = float('Inf')
    
    while len(V) > 0:
        center = S[-1]  # get final node for S as the new center node
        minval = ("None", float("Inf"))
        for node, d in graphdict[center]:
            if node not in V:
                continue
            if len(S) == 1:
                dist[node] = d
            else:
                startToCenterDist = dist[center]
                if startToCenterDist + d < dist[node]:
                    dist[node] = startToCenterDist + d

            if d < minval[1]:
                minval = (node, d)
        V.remove(minval[0])
        S.append(minval[0])  # append node with min val
    return dist


shortestRoad = Dijkstra(
    "A",
    graphdict={
        "A": [("B", 6), ("C", 3)],
        "B": [("C", 2), ("D", 5)],
        "C": [("B", 2), ("D", 3), ("E", 4)],
        "D": [("B", 5), ("C", 3), ("E", 2), ("F", 3)],
        "E": [("C", 4), ("D", 2), ("F", 5)],
        "F": [("D", 3), ("E", 5)]
    })

mystr = "shortest distance from A begins to "

for key, shortest in shortestRoad.items():

    print(mystr + str(key) + " is: " + str(shortest))
