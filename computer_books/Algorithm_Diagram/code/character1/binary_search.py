def binary_search(list, item):
    low = 0
    high = len(list) - 1
    print("hight value:", high)
    while low <= high:
        mid=(low+high)//2
        print(mid)
        print("mid value:",list[mid])
        guess = list[mid]
        if guess == item:
            return mid
        if guess > item:
            high = mid - 1
        else:
            low = mid + 1
    return 0


my_list = [1, 2, 3, 5, 6, 11, 12, 19, 20]
print(binary_search(my_list, 5))
