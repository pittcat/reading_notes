#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Shape {
public:
  enum ShapeColor { Red, Green, Blue };
  virtual void draw(ShapeColor color = Red) const = 0;
};

class Rectangle : public Shape {
public:
  virtual void draw(ShapeColor color = Green) const { cout << color << endl; };
};

class Circle : public Shape {
public:
  virtual void draw(ShapeColor color=Blue) const { cout << color << endl; };
};

int main() {
  Shape *ps;
  Shape *pr = new Rectangle;
  Shape *pc = new Circle;
  pr->draw();
  pc->draw();
  return 0;
}
