// Person 接口
// string& getName() const;
// void setName(string& name);
// int getAge() const;
// void setAge(int age);
// 它们的功能是设置获取名字和年龄。

#ifndef _PERSON_H_
#define _PERSON_H_
#include <string>

class PersonImpl;

using namespace std;

class Person {
    public:
  Person(string &name, int age);
  virtual ~Person();
  string &getName() const;
  void setName(string &name);
  int getAge() const;
  void setAge(int age);

private:
  PersonImpl *mPersonImpl;
};

#endif /* !PERSON_H */
