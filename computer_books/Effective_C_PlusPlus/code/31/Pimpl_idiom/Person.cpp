#include "Person.h"
#include "PersonImpl.h"

Person::Person(string &name, int age) : mPersonImpl(new PersonImpl(name, age)) {

  std::cout << "construct Person" << std::endl;
}

Person::~Person() {
  delete mPersonImpl;
  std::cout << "deconstruct Person" << std::endl;
}

string &Person::getName() const { return mPersonImpl->getName(); }

void Person::setName(string &name) { mPersonImpl->setName(name); }

int Person::getAge() const { return mPersonImpl->getAge(); }

void Person::setAge(int age) { mPersonImpl->setAge(age); }
