#include <string>
#include <iostream>

using namespace std;

class PersonImpl {
    public:
        PersonImpl(string &name,int age);
        virtual ~PersonImpl();
        string & getName() const;
        void setName(string & name);
        int getAge() const;
        void setAge(int age);
    private:
        string &mName;
        int mAge;

};
