#include "PersonImpl.h"
#include <string>

PersonImpl::PersonImpl(string &name, int age) : mName(name), mAge(age) {}

PersonImpl::~PersonImpl(){

};

string &PersonImpl::getName() const { return mName; };

void PersonImpl::setName(string &name) { mName = name; }

int PersonImpl::getAge() const { return mAge; }

void PersonImpl::setAge(int age) { mAge = age; };
