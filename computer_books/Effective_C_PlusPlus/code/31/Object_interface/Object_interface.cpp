#include <iostream>
#include <memory>
#include <string>

using namespace std;

class Date {};

class Address {};

class Person {
public:
  virtual ~Person();
  virtual string name() const = 0;
  virtual string birthDate() const = 0;
  virtual string address() const = 0;

  static std::shared_ptr<Person>
  create(const string &name, const Date &birthday, const Address &addr);
};

class RealPerson : public Person {
public:
  RealPerson(const string &name, const Date &birthday, const Address &addr)
      : theName(name), theBirthDate(birthday), theAddress(addr) {}
  virtual ~RealPerson(){};
  string name() const;
  string birthDate() const;
  string address() const;

private:
  string theName;
  Date theBirthDate;
  Address theAddress;
};

std::shared_ptr<Person> Person::create(const string &name, const Date &birthday,
                                       const Address &addr) {
  return shared_ptr<Person>(new RealPerson(name, birthday, addr));
};

int main() {
  // Person usage
  string name;
  Date dateOfbBirth;
  Address address;
  std::shared_ptr<Person> pp(Person::create(name, dateOfbBirth, address));
  // call by Person's interface
  cout << pp->name() << "was born on " << pp->birthDate()
       << " and now lives at " << pp->address();

  return 0;
}
