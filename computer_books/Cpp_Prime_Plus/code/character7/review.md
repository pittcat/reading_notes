#### 1

这3个步骤是定义函数、提供原型、调用函数。

#### 2

```cpp
void igor(void);
float tofu(int n);
double mpg(double miles,double gallons);
long summation(long ar[],int n);
double docor(const char *str);
void ofcourse(boss dude);
char * plot(map *pmap);
```

#### 3

```cpp
void set_array(int arr[],int size,int value){
  for (int i = 0;i < size; i++){
    arr[i] = value;
  }
}
```

#### 4
```cpp
void set_array(int *begin,int *end,int value){
  for (int * pt = begin; pt ! = end; pt++)
    pt*=value;
}
```

#### 5
```cpp
double max_ar(double ar[], int n) {
  double max_n = ar[0];
  for (int i = 0; i < n; ++i) {
    if (ar[i] > max_n) {
      max_n = ar[i];
    }
  }
  return max_n;
}
```

#### 6

将const限定符用于指针，以防止指向的原始数据被修改。程序传递基本类型（如int或double）时，它将按值传递，以便函数使用副本。这样，原始数据将得到保护.

#### 7
字符串可以存储在char数组中，可以用带双引号的字符串来表示，也可以用指向字符串第一个字符的指针来表示。

#### 8

```cpp
int replace(char *str, char c1, char c2) {
  int count = 0;
  while (*str) {
    if (*str == c1) {
      count++;
      *str = c2;
    }
    str++;
  }
  return count;
}
```

#### 9

p,c

#### 10
要按值传递它，只要传递结构名glitz即可。要传递它的地址，请使用地址运算符&glitz。按值传递将自动保护原始数据，但这是以时间和内存为代价的。按地址传递可节省时间和内存，但不能保护原始数据，除非对函数参数使用了const限定符。另外，按值传递意味着可以使用常规的结构成员表示法，但传递指针则必须使用间接成员运算符。

#### 11

```cpp
int judge(int (*pf) (const char *));
```
#### 12

```cpp
void display1(applicant app) {
  cout << app.name;
  cout << endl;
  cout << app.credit_ratings;
};

void display2(applicant *app) {
  cout << app->name << endl;
  cout << app->credit_ratings;
}

```

#### 13

```cpp
typedef void (*p_f1)(applicant *);
p_f1 p1 = f1;
typedef const char * (*p_f2)(const applicant *,const applicant *);
p_f2 p2 = f2;
p_f1 ap[5];
p_f2 (*pa)[10];
```
