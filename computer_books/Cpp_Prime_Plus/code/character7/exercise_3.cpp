#include <iostream>
#include <string>
#include <vector>

using namespace std;
struct box {
  char maker[40];
  float height;
  float width;
  float length;
  float volume;
};

void show_sbox(box n);
void set_volume(box *n);

int main() {
  box b = {};
  show_sbox(b);
  set_volume(&b);
  return 0;
}

void show_sbox(box n) {
  cout << n.maker<<endl;
  cout << n.height<<endl;
  cout << n.width<<endl;
  cout << n.length<<endl;
  cout << n.volume<<endl;
}



void set_volume(box *n){
    n->volume=n->height*n->length*n->width;
}
