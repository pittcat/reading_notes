#include <iostream>
#include <string>
#include <vector>

using namespace std;
int sum_arr(const int *begin, const int *end);
const int Arsize = 8;

int main() {
  int cookies[Arsize] = {1, 3, 4, 5, 6, 4, 5, 6};
  int sum = sum_arr(cookies, cookies + Arsize);
  cout << "Total cookies eaten: " << sum << endl;
  sum = sum_arr(cookies, cookies + 3);
  cout << "First three eaters ate " << sum << " cookies.\n";
  sum = sum_arr(cookies, cookies + 8);
  cout << "Last four eaters ate " << sum << " cookies.\n";
  return 0;
}

int sum_arr(const int *begin, const int *end) {
  const int *pt;
  int total = 0;
  for (pt = begin; pt != end; pt++) {
    total += *pt;
  }
  return total;
}
