#include <iostream>
#include <string>
#include <vector>

using namespace std;
int sum_arr(int arr[], int n);
const int Arsize = 8;
int main() {
  int cookies[Arsize] = {1, 3, 4, 5, 6, 7, 8, 9};
  cout << cookies << " = array address, ";
  cout << sizeof(cookies) << " = sizeof cookies\n";
  int sum = sum_arr(cookies, Arsize);
  cout << "Total cookies eaten: " << sum << endl;
  sum = sum_arr(cookies, 3);
  cout << "First three eaters ate " << sum << " cookies.\n";
  sum = sum_arr(cookies + 4, 4);
  cout << "Last four eaters ate " << sum << " cookies.\n";

  return 0;
}

int sum_arr(int arr[], int n) {
  int total = 0;
  cout << arr << " = arr, ";
  cout << sizeof arr << " = sizeof arr\n";
  for (int i = 0; i < n; ++i) {
    total += arr[i];
  }
  return total;
}
