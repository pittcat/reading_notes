#include <iostream>
#include <string>
#include <vector>

using namespace std;

double HarmonicMean(double, double);
int main() {
  int n1, n2;
  cout << "Please enter a number1:";
  cin >> n1;
  cout << "Please enter a number2:";
  cin >> n2;
  while (n1 * n2) {
    cout << HarmonicMean(n1, n2) << endl;
    cout << "Please enter a number1:";
    cin >> n1;
    cout << "Please enter a number2:";
    cin >> n2;
  }
  cout << "Bye!\n";
  return 0;
}

double HarmonicMean(double x, double y) {
  double harmicmean = 0.0;
  harmicmean = 2.0 * x * y / (x + y);
  return harmicmean;
}
