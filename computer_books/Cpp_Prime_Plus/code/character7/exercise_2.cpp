#include <iostream>
#include <string>
#include <vector>

using namespace std;
const int Max = 10;
int main() {
  int count = 0;
  double ar[Max], sum = 0;
  double average;
  cout << "Init Golf Score" << endl;
  for (int i = 0; i < Max; ++i) {
    if (cin >> ar[i]) {
      sum += ar[i];
      count++;
    } else {
      break;
    }
  }
  cout << "Show Golf Score" << endl;
  for (int i = 0; i < count; ++i) {
    cout << ar[i] << endl;
  }
  average = sum / count;
  cout << "Average:" << average;
  return 0;
}
