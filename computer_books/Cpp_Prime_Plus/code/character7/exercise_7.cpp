#include <iostream>
#include <string>
#include <vector>

using namespace std;
double *Fill_array(double ar[], int size);
void Show_array(double *start, double *end);
void Reverse_array(double *start, double *end);

const int Max_size = 20;

int main() {
  double array[Max_size], *arr_end;

  cout << "Init Array:" << endl;
  arr_end = Fill_array(array, Max_size);
  Show_array(array, arr_end);
  Reverse_array(array, arr_end);
  Show_array(array, arr_end);
  return 0;
}

double *Fill_array(double ar[], int size) {
  for (int i = 0; i < size; ++i) {
    cout << "Num " << i << " :";
    if (cin >> *ar ) {
        ar++;
    } else {
      break;
    }
  }
  return ar-1;
}

void Show_array(double *start, double *end) {
  cout << "Array Content:" << endl;
  while (start <= end) {
    cout << *start<<endl;
    start++;
  }
}

void Reverse_array(double *start, double *end) {
  double temp;
  while (start < end) {
    temp = *start;
    *start = *end;
    *end = temp;
    start++;
    end--;
  }
}
