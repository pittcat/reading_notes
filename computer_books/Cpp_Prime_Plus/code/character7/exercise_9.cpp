#include <iostream>
#include <string>
#include <vector>

using namespace std;
const int SLEN = 30;

struct student {
  char fullname[SLEN];
  char hobby[SLEN];
  int ooplevel;
};

int getinfo(student pa[], int n);
void display1(student st);
void display2(student *st);
void display3(const student pa[], int n);

int main() {
  cout << "Enter class size:";
  int class_size;
  cin >> class_size;
  while (cin.get() != '\n') {
    continue;
  }
  student *ptr_stu = new student[class_size];
  int entered = getinfo(ptr_stu, class_size);
  for (int i = 0; i < entered; ++i) {
    display1(ptr_stu[i]);
    display2(&ptr_stu[i]);
  }
  display3(ptr_stu, entered);
  delete[] ptr_stu;
  cout << "Done!\n";
  return 0;
}

int getinfo(student pa[], int n) {
  int count = 0;
  cout << "Please input student info\n";
  for (int i = 0; i < n; ++i) {
    cout << "Student " << i + 1 << endl;
    cout << "name:";
    cin >> pa[i].fullname;
    if (pa[i].fullname[0]=='\n') {
      break;
    }
    cin.get();
    cout << "hobby:";
    cin >> pa[i].hobby;
    cout << "ooplevel:";
    cin >> pa[i].ooplevel;
    count++;
  }
  return count;
}

void display1(student st) {
  cout << "name:" << st.fullname << " hobby:" << st.hobby
       << " ooplevel:" << st.ooplevel<<endl;
}

void display2(student *st) {
  cout << "name:" << st->fullname << " hobby:" << st->hobby
       << " ooplevel:" << st->ooplevel<<endl;
}

void display3(const student pa[], int n) {
  for (int i = 0; i < n; ++i) {
    cout << "name:" << pa[i].fullname << " hobby:" << pa[i].hobby
         << " ooplevel:" << pa[i].ooplevel<<endl;
  }
}
