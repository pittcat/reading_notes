#include <iostream>
#include <string>
#include <vector>

using namespace std;
char *buildstr(char c, int n);
int main() {
  int times;
  char ch;
  cout << "Enter a character: ";
  cin >> ch;
  cout << "Enter an character: ";
  cin >> times;
  char *ps = buildstr(ch, times);
  delete[] ps;
  ps = buildstr('+', 20);
  cout << ps << "-DONE-" << ps << endl;
  delete[] ps;
  return 0;
}

char *buildstr(char c, int n) {
  char *pstr = new char[n + 1];
  pstr[n] = '\n';
  while (n-- > 0) {
    pstr[n] = c;
  }
  return pstr;
}
