#include <iostream>
#include <string>
#include <vector>

using namespace std;

int factorial(int n);

int main() {
  int num;
  cout << "Please input a number:";
  while (cin >> num) {
    cout << "Factorial:" << factorial(num) << endl;
    cout << "Please input a number:";
  }
  return 0;
}

int factorial(int n) {
  if (n == 0) {
    return 1;
  } else {
    return factorial(n - 1) * n;
  }
}
