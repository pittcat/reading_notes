#include <iostream>
#include <string>
#include <vector>

using namespace std;
int Fill_array(double ar[], int size);
void Show_array(double ar[], int size);
void Reverse_array(double ar[], int size);

const int Max_size = 20;

int main() {
  double array[Max_size];
  int n_count;
  cout << "Init Array" << endl;
  n_count = Fill_array(array, Max_size);
  Show_array(array, n_count);
  Reverse_array(array, n_count);
  Show_array(array, n_count);
  return 0;
}

int Fill_array(double ar[], int size) {
  int cur;
  cur = 0;
  cout << "Input number " << cur << ": ";
  while (cin >> ar[cur] && cur < size) {
    cur++;
    cout << "Input number " << cur << ": ";
  }
  return cur;
}

void Show_array(double ar[], int size) {
  cout << "Array Content:" << endl;
  for (int i = 0; i < size; ++i) {
    cout << ar[i] << endl;
  }
}

void Reverse_array(double ar[], int size) {
  double temp;
  int disdance = size - 1;
  for (int i = 0; i < size / 2; ++i) {
    temp = ar[i];
    ar[i] = ar[i + disdance];
    ar[i + disdance] = temp;
    disdance -= 2;
  }
}
