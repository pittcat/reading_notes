#include <iostream>
#include <string>
#include <vector>

using namespace std;
int sum_arr(int arr[], int n);
const int Arsize = 8;
int main() {
  int cookies[Arsize] = {1, 3, 4, 5, 6, 7, 8, 9};
  int sum = sum_arr(cookies, Arsize);
  cout << "Total cookies eaten: " << sum << "\n";
  return 0;
}

int sum_arr(int arr[], int n) {
  int total = 0;
  for (int i = 0; i < n; ++i) {
    total += arr[i];
  }
  return total;
}
