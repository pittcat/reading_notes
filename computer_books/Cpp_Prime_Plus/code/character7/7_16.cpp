#include <iostream>
#include <string>
#include <vector>

using namespace std;
void countdown(int n);

int main() {
  countdown(4);
  return 0;
}

void countdown(int n) {
  cout << "Countind down ..." << n << endl;
  if (n > 0) {
    countdown(n - 1);
  }
  cout << n << ": Kaboom!\n";
}
