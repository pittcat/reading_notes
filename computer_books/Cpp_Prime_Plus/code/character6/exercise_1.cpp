#include <cctype>
#include <cstdio>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

using namespace std;
const int C_size = 120;

int main() {
  char c[C_size];
  int cur = 0;
  cout << "Input:";
  c[cur] = getchar();
  while (c[cur] != '@' && cur < C_size - 1) {
    cur++;
    c[cur] = getchar();
  }
  c[cur] = '\0';
  cout << c;
  for (int i = 0; i < C_size && c[i] != '\0'; ++i) {
    if (isdigit(c[i])) {
      continue;
    } else if (islower(c[i])) {
      cout << (char)toupper(c[i]);
    } else if (isupper(c[i])) {
      cout << (char)tolower(c[i]);
    } else {
      cout << c[i];
    }
  }
  return 0;
}
