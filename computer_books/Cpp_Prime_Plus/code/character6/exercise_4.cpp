#include <iostream>
#include <string>
#include <vector>

using namespace std;
const int strsize = 100;

struct bop {
  char fullname[strsize];
  char title[strsize];
  char bopname[strsize];
  int preference;
};
void dname(bop *, int);
void dtitle(bop *, int);
void dbopname(bop *, int);
void dprefernce(bop *, int);
void showmenu();

int main() {
  bop programer[4]{{"Wimp Macho", "doctor", "K", 1},
                   {"Raki Rhodes", "bachelor", "O", 1},
                   {"Celiaa Laiter", "Associate Professor", "L", 1},
                   {"Hoppy Hipman", "doctor", "K", 1}};
  char choice;
  showmenu();
  cout << "Enter your choice:";
  cin >> choice;
  while (choice != 'q') {
    switch (choice) {
    case 'a':
      dname(programer, 4);
      break;
    case 'b':
      dtitle(programer, 4);
      break;
    case 'c':
      dbopname(programer, 4);
      break;
    case 'd':
      dprefernce(programer, 4);
      break;
    default:
      break;
    }
    cout << "Next Choice:";
    cin >> choice;
  }
  cout << "Bye!";
  return 0;
}

void showmenu() {
  cout << "Benevolent Order of Programmers Report\n";
  cout << "a. display by name       b. display by title\n";
  cout << "c. display by bopname    d. display by preference\n";
  cout << "q. quit\n";
}

void dname(bop *pra, int size) {
  for (int i = 0; i < size; ++i) {
    cout << pra[i].fullname << endl;
  }
}

void dtitle(bop *pra, int size) {
  for (int i = 0; i < size; ++i) {
    cout << pra[i].title << endl;
  }
}

void dbopname(bop *pra, int size) {
  for (int i = 0; i < size; ++i) {
    cout << pra[i].bopname << endl;
  }
}

void dprefernce(bop *pra, int size) {
  for (int i = 0; i < size; ++i) {
    if (pra[i].preference) {
      cout << pra[i].title << endl;
    }
  }
}
