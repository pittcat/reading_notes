#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct donors {
  string name;
  double money;
};

int main() {
  int num;
  cout << "Please input donors number:";
  donors *donor_people = new donors[num];
  // init donors info
  for (int i = 0; i < num; ++i) {
    cout << "donor " << i << "name: ";
    cin >> donor_people[i].name;
    cout << "donor " << i << "money: ";
    cin >> donor_people[i].money;
  }
  // Grand Patrons
  cout << "Grand Patrons:\n";
  for (int i = 0; i < num; ++i) {
    if (donor_people[i].money > 10000) {
      cout << donor_people[i].name << "---" << donor_people[i].money << endl;
    }
  }
  cout << "Patrons:\n";
  for (int i = 0; i < num; ++i) {
    if (donor_people[i].money < 10000) {
      cout << donor_people[i].name << "---" << donor_people[i].money << endl;
    }
  }
  return 0;
}
