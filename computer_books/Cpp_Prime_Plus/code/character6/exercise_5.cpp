#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  double tvarp, tax;
  cout << "Please input tvarp:";
  while (cin >> tvarp && tvarp > 0) {
    if (tvarp <= 5000) {
      tax = 0;
    } else if (tvarp > 5000 && tvarp <= 15000) {
      tvarp -= 5000l;
      tax = tvarp * 0.1;
    } else if (tvarp > 15000 && tvarp <= 35000) {
      tvarp -= 15000;
      tax = 10000 * 0.1 + tvarp * 0.15;
    } else {
      tvarp -= 35000;
      tax = 10000 * 0.1 + 20000 * 0.15 + tvarp * 0.2;
    }
    cout<<"Tax: "<<tax;
    cout << "Please input tvarp:";
  }
  return 0;
}
