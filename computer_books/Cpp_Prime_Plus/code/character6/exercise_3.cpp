#include <iostream>
#include <string>
#include <vector>

using namespace std;

void showmenu();
int main() {
  char input;
  showmenu();
  cout << "Please enter a c, p, t, or g:";
  cin >> input;
  switch (input) {
  case 'c':
    cout << "Tiger is a carnivore.";
    break;
  case 'p':
    cout << "Beethoven is a pianist.";
    break;
  case 't':
    cout << "A maple is a tree.";
    break;
  case 'g':
    cout << "LOL is a game.";
    break;
  default:
    break;
  }
  return 0;
}

void showmenu() {
  cout << "Please enter one of the following choices:\n";
  cout << "c) carnivore     p) pianist\n";
  cout << "t) tree          g) game\n";
}
