cmake_minimum_required(VERSION 2.8)

# projectname is the same as the main-executable 项目信息
project(usetime)

# 指定生成目标
add_executable(main main.cpp dma.cpp)

set(CMAKE_BUILD_TYPE "Debug")
set(CMAKE_CXX_FLAGS_DEBUG "$ENV{CXXFLAGS} -O0 -Wall -g2 -ggdb")
set(CMAKE_CXX_FLAGS_RELEASE "$ENV{CXXFLAGS} -O3 -Wall")
