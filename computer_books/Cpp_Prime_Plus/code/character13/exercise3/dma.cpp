#include "dma.h"
#include <cstring>
#include <iostream>
#include <ostream>
#include <string>
#include <vector>

using namespace std;

ABC::ABC(const char *l, int r) {
  label = new char[strlen(l) + 1];
  strcpy(label, l);
  rating = r;
}

ABC::ABC(const ABC &a) {
  label = new char[strlen(a.label) + 1];
  strcpy(label, a.label);
  rating = a.rating;
}

ABC::~ABC() { delete[] label; }

void ABC::View() const {
  cout << "label: " << label << endl;
  cout << "rating: " << rating << endl;
}

ABC &ABC::operator=(const ABC &a) {
  if (this == &a) {
    return *this;
  }
  delete[] label;
  label = new char[strlen(a.label) + 1];
  strcpy(label, a.label);
  rating = a.rating;
  return *this;
}

ostream &operator<<(ostream &os, const ABC &a) {
  os << a.label << "," << a.rating;
  return os;
}

/****
 * baseDMA *
 ****/
baseDMA::baseDMA(const char *l, int r) : ABC(l, r) {}

ostream &operator<<(ostream &os, const baseDMA &rs) {
  os << (const ABC &)rs;
  return os;
}

/******
 *  lacksDMA  *
 ******/

lacksDMA::lacksDMA(const char *c, const char *l, int r) : ABC(l, r) {
  strcpy(color, c);
}

lacksDMA::lacksDMA(const char *c, const ABC &a) : ABC(a) { strcpy(color, c); }

void lacksDMA::View() const {
  ABC::View();
  cout << "color:" << color << endl;
}

ostream &operator<<(ostream &os, const lacksDMA &rs) {
  os << rs.color << ", " << (const ABC &)rs;
  return os;
}

/****
 * hasDMA method *
 ****/

hasDMA::hasDMA(const char *s, const char *l, int r) : ABC(l, r) {
  style = new char[strlen(s) + 1];
  strcpy(style, s);
}

hasDMA::hasDMA(const char *s, const ABC &c) : ABC(c) {
  style = new char[strlen(s) + 1];
  strcpy(style, s);
}

hasDMA::~hasDMA() { delete[] style; }

void hasDMA::View() const {
  ABC::View();
  cout << style << endl;
}

hasDMA &hasDMA::operator=(const hasDMA &rs) {
  if (this == &rs) {
    return *this;
  }
  ABC::operator=(rs);
  delete[] style;
  style = new char[strlen(rs.style) + 1];
  strcpy(style, rs.style);
  return *this;
}

ostream &operator<<(ostream &os, const hasDMA &rs) {
  os << rs.style << ", " << (const ABC &)rs;
  return os;
}
