#include "dma.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  baseDMA shirt("Portrabelly", 8);
  lacksDMA balloon("red", "Blumpo", 4);
  hasDMA map("Mercator", "Buffalo", 5);
  cout << shirt << endl;
  cout << balloon << endl;
  cout << map << endl;

  lacksDMA ballon2(balloon);
  hasDMA map2;
  map2 = map;
  cout << ballon2 << endl;
  cout << map2 << endl;

  ABC *pts[3];
  pts[0] = &shirt;
  pts[1] = &balloon;
  pts[2] = &map;
  cout << "cycle:" << endl;
  for (int i = 0; i < 3; ++i) {
    cout << *pts[i] << endl;
  }

  cout << "pointer call func:" << endl;
  for (int i = 0; i < 3; ++i) {
    pts[i]->View();
  }

  return 0;
}
