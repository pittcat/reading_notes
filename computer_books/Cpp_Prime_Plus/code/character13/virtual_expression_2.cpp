#include <iostream>

using namespace std;

//首先，我们定义了A类
class A {
public:
  virtual ~A() { cout << "~A()" << endl; }

private:
  char *a;
};

//然后定义B类，B类继承自A类
class B : public A {
public:
  ~B() { cout << "~B()" << endl; }

private:
  char *b;
};

int main() {
  A *p = new B;
  delete p;
  return 0;
}
