#include "port.h"
#include <cstring>
#include <iostream>
#include <ostream>

using namespace std;

Port::Port(const char *br, const char *st, int b) {
  brand = new char[strlen(br) + 1];
  strcpy(brand, br);
  strcpy(style, st);
  bottles = b;
}

Port::Port(const Port &p) {
  brand = new char[strlen(p.brand) + 1];
  strcpy(brand, p.brand);
  strcpy(style, p.style);
  bottles = p.bottles;
}

Port &Port::operator=(const Port &p) {
  if (this == &p) {
    return *this;
  }
  delete[] brand;
  brand = new char[strlen(p.brand) + 1];
  strcpy(brand, p.brand);
  strcpy(style, p.style);
  bottles = p.bottles;
  return *this;
}

Port &Port::operator+=(int b) {
  this->bottles += b;
  return *this;
}

Port &Port::operator-=(int b) {
  if (this->bottles - b > 0) {
    this->bottles -= b;
    return *this;
  } else {
    return *this;
  }
}

void Port::Show() const {
  cout << "Brand: " << brand << endl;
  cout << "Kind: " << style << endl;
  cout << "Bottles: " << bottles << endl;
}

ostream &operator<<(ostream &os, const Port &p) {
  os << "Brand: " << p.brand << ", "
     << "Kind: " << p.style
     << ", "
        "Bottles: "
     << p.bottles << endl;
  return os;
}

/****
 * VintagePort *
 ****/

VintagePort::VintagePort(const char *br, int b, const char *nm, int y)
    : Port(br, "vintage", b) {
  nickname = new char[strlen(nm) + 1];
  strcpy(nickname, nm);
  year = y;
}

VintagePort::VintagePort(const VintagePort &vp) : Port(vp) {

  nickname = new char[strlen(vp.nickname) + 1];
  strcpy(nickname, vp.nickname);
  year = vp.year;
}

void VintagePort::Show() const {
  Port::Show();
  cout << "Nickname: " << nickname << endl;
  cout << "Year: " << year << endl;
}

VintagePort &VintagePort::operator=(const VintagePort &p) {
  if (this == &p) {
    return *this;
  }

  Port::operator=(p);
  delete[] nickname;
  strcpy(nickname, p.nickname);
  year = p.year;

  return *this;
}

ostream &operator<<(ostream &os, const VintagePort &vp) {
  os << (const Port &)vp << ", " << vp.nickname << ", " << vp.year;
  return os;
}
