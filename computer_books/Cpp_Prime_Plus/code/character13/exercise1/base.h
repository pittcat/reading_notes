#ifndef BASE_H
#define BASE_H

class Cd {
private:
  char performers[50];
  char label[20];
  int selections;
  double playtime;

public:
  Cd(char *s1, char *s2, int n, double x);
  Cd();
  Cd & operator=( Cd const &d);
  virtual ~Cd(){};
  virtual void Report() const;
};

class Classic : public Cd {
private:
    char production[100];
public:
  Classic(char *pr,char *s1, char *s2, int n, double x);
  Classic();
  Classic &operator=(const Classic &c);
  virtual ~Classic(){};
  virtual void Report() const;
};

#endif /* BASE_H */
