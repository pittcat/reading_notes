#include "base.h"
#include <cstdio>
#include <cstring>
#include <iostream>

using namespace ::std;

Cd::Cd(char *s1, char *s2, int n, double x) {
  strcpy(performers, s1);
  strcpy(label, s2);
  selections = n;
  playtime = x;
}

Cd &Cd::operator=(const Cd &d) {
  if (&d == this) {
    return *this;
  }
  strcpy(performers, d.performers);
  strcpy(label, d.label);
  selections = d.selections;
  playtime = d.playtime;
  return *this;
}

Cd::Cd() {
  performers[0] = '\0';
  label[0] = '\0';
  selections = 0;
  playtime = 0.0;
}

void Cd::Report() const {
  cout << "performers:" << performers << endl;
  cout << "label:" << label << endl;
}

Classic::Classic(char *pr, char *s1, char *s2, int n, double x)
    : Cd(s1, s2, n, x) {
  strcpy(production, pr);
}

Classic::Classic() { production[0] = '\0'; }

void Classic::Report() const {
  Cd::Report();
  cout << "production: " << production << std::endl;
}

Classic &Classic::operator=(const Classic &c) {
  if (this == &c) {
    return *this;
  }
  Cd::operator=(c);
  strcpy(production, c.production);
  return *this;
}
