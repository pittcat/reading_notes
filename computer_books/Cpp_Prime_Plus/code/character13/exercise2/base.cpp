#include "base.h"
#include <cstdio>
#include <cstring>
#include <iostream>

using namespace ::std;

Cd::Cd(char *s1, char *s2, int n, double x) {
  performers = new char[strlen(s1) + 1];
  label = new char[strlen(s2) + 1];
  strcpy(performers, s1);
  strcpy(label, s2);
  selections = n;
  playtime = x;
}

Cd::Cd(const Cd &d) {
  performers = new char[strlen(d.performers) + 1];
  strcpy(performers, d.performers);
  label = new char[strlen(d.label) + 1];
  selections = d.selections;
  playtime = d.playtime;
}

Cd::Cd() {
  performers = nullptr;
  label = nullptr;
  selections = 0;
  playtime = 0.0;
}
Cd::~Cd() {
  delete[] performers;
  delete[] label;
};

Cd &Cd::operator=(const Cd &d) {
  if (&d == this) {
    return *this;
  }

  delete[] performers;
  performers = new char[strlen(d.performers) + 1];
  strcpy(performers, d.performers);
  delete[] label;
  label = new char[strlen(d.label) + 1];
  strcpy(label, d.label);

  selections = d.selections;
  playtime = d.playtime;
  return *this;
}
void Cd::Report() const {
  cout << "performers:" << performers << endl;
  cout << "label:" << label << endl;
  std::cout << "selections: " << selections << std::endl;
  std::cout << "playtime: " << playtime << std::endl;
}

Classic::Classic(char *pr, char *s1, char *s2, int n, double x)
    : Cd(s1, s2, n, x) {
  production = new char[strlen(pr) + 1];
  strcpy(production, pr);
}

Classic::Classic() { production = nullptr; }

Classic::~Classic() { delete[] production; }

void Classic::Report() const {
  Cd::Report();
  cout << "production:" << production << endl;
}

Classic &Classic::operator=(const Classic &c) {
  if (this == &c) {
    return *this;
  }
  Cd::operator=(c);
  delete[] production;
  production = new char[strlen(c.production) + 1];
  strcpy(production, c.production);
  return *this;
}
