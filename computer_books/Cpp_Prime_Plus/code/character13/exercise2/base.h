#ifndef BASE_H
#define BASE_H

class Cd {
private:
  char *performers;
  char *label;
  int selections;
  double playtime;

public:
  Cd(char *s1, char *s2, int n, double x);
  Cd(const Cd &d);
  Cd();
  Cd &operator=(Cd const &d);
  virtual ~Cd();
  virtual void Report() const;
};

class Classic : public Cd {
private:
  char *production;

public:
  Classic(char *pr, char *s1, char *s2, int n, double x);
  Classic(const Classic &c);
  Classic();
  Classic &operator=(const Classic &c);
  virtual ~Classic();
  virtual void Report() const;
};

#endif /* BASE_H */
