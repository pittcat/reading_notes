#include "tabtenn0.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  TableTennisPlayer player1("Chuck", "Blizzard", true);
  TableTennisPlayer player2("Tara", "Boomdea", false);
  player1.Name();
  if (player1.HasTable()) {
    cout << ": has a table.\n";
  } else {
    cout << ": hasn't a table";
  }
  player2.Name();
  if (player2.HasTable()) {
    cout << ": has a table.\n";
  } else {
    cout << ": hasn't a table";
  }
  return 0;
}
