#include "coordin.h"
#include <cmath>
#include <iostream>

using namespace std;
polar rect_to_polar(rect xypos) {
  polar answer;
  answer.disdance = sqrt(xypos.x * xypos.x + xypos.y * xypos.y);
  answer.angle = atan2(xypos.y, xypos.x);
  return answer;
}

void show_polar(polar dapos) {
  const double Rad_to_deg = 57.29577;
  cout << "distance = " << dapos.disdance;
  cout << ", angle = " << dapos.angle * Rad_to_deg;
  cout << " degree\n";
}
