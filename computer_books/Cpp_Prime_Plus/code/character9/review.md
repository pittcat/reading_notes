#### 1

a．homer 将自动成为自动变量。
b．应该在一个文件中将 secret 定义为外部变量，并在第二个文件中使用 extern 来声明它。
c．可以在外部定义前加上关键字 static，将 topsecret 定义为一个有内部链接的静态变量。也可在一个未命名的名称空间中进行定义。
d．应在函数中的声明前加上关键字 static，将 beencalled 定义为一个本地静态变量。

#### 2

using 声明使得名称空间中的单个名称可用，其作用域与 using 所在的声明区域相同。using 编译指令使名称空间中的所有名称可用。使用 using 编译指令时，就像在一个包含 using 声明和名称空间本身的最小声明区域中声明了这些名称一样。

#### 3

```cpp
#include <iostream>

int main(int argc, char *argv[]) {
  double x;
  std::cout << "Enter value:";
  while (!(std::cin >> x)) {
    std::cout << "Bad input. Please enter a number:";
    std::cin.clear();
    while (std::cin.get() != '\n') {
      continue;
    }
  }
  std::cout << "Value = " << x << std::endl;
  return 0;
}
```

#### 4

```cpp
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
int main(int argc, char *argv[]) {
  double x;
  cout << "Enter value: ";
  while (!(cin >> x)) {
    cout << "Bad input. Please enter a number: ";
    cin.clear();
    while (cin.get() != '\n') {
      continue;
    }
  }
  cout << "Value = " << x << endl;
  return 0;
}
```

#### 5

可以在每个文件中包含单独的静态函数定义。或者每个文件都在未命名的名称空间中定义一个合适的 average( )函数。

#### 6

10
4
0
Other: 10,1
another(): 10,-4

#### 7

1
4, 1, 2
2
2
4, 1, 2
2
