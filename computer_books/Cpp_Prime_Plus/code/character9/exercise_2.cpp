#include <iostream>
#include <string>
#include <vector>

using namespace std;

void strcount(string str);

int main() {
  string input;
  cout << "Enter a line:\n";
  getline(cin, input);
  while (input.compare("") != 0) {
    strcount(input);
    cout << "Enter a line(empty line to quit):\n";
    getline(cin, input);
  }
  cout << "Bye\n";
  return 0;
}

void strcount(string str) {
  static int total = 0;
  int count = 0;
  cout << "\"" << str << "\" contains ";
  count = str.size();
  total += count;
  cout << count << " characters\n";
  cout << total << " characters total\n";
}
