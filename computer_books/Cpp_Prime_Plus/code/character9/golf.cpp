#include "golf.h"
#include <cstring>
#include <iostream>

void setgolf(golf &g, const char *name, int hc) {
  strcpy(g.fullname, name);
  g.handicap = hc;
}

int setgolf(golf &g) {
  std::cout << "Please enter fullname:";
  std::cin.getline(g.fullname, Len);
  if (g.fullname[0]) {
    std::cout << "Please enter handicap:";
    std::cin >> g.handicap;
    return 1;
  } else {
    return 0;
  }
}

void handicap(golf &g, int hc) { g.handicap = hc; }

void showgolf(const golf &g) {
  std::cout << "Golf info\n";
  std::cout << "Fullname: " << g.fullname << std::endl;
  std::cout << "Handicap: " << g.handicap << std::endl;
}
