#### 1

a. friend class clasp;
b.

```cpp
class cuff {
  public:
    class muff;
    void snip(muff &){
      ...
    }
};
class muff {
  friend void cuff::snip(muff &);
  ...
}
```

c.

```cpp
class muff;
class cuff {
public:
  void snip(muff &) {...};
  class muff {
    friend void cuff::snip(muff &);
    ...
  }
}
```

#### 2

不。为使类 A 拥有一个本身为类 B 的成员函数的友元，B 的声明必须位于 A 的声明之前。一个前向声明是不够的，因为这种声明可以告诉 A:B 是一个类；但它不能指出类成员的名称。同样，如果 B 拥有一个本身是 A 的成员函数的友元，则 A 的这个声明必须位于 B 的声明之前。这两个要求是互斥的。

#### 3

访问类的唯一方法是通过其有接口，这意味着对于 Sauce 对象，只能调用构造函数来创建一个。其他成员 (soy 和 sugar) 在默认情况下是私有的。

#### 4

假设函数 f1( ) 调用函数 f2( )。f2( ) 中的返回语句导致程序执行在函数 f1( ) 中调用函数 f2( ) 后面的一条语句。throw 语句导致程序沿函数调用的当前序列回溯，直到找到直接或间接包含对 f2( ) 的调用的 try 语句块为止。它可能在 f1( ) 中、调用 f1( ) 的函数中或其他函数中。找到这样的 try 语句块后，将执行下一个匹配的 catch 语句块，而不是函数调用后的语句。

#### 5

应按从子孙到祖先的顺序排列 catch 语句块。

#### 6

对于示例#1, 如果 pg 指向一个 Superb 对象或从 Superb 派生而来的任何类的对象，则 if 条件为 true。具体地说，如果 pg 指向 Magnificent 对象，则 if 条件也为 true。对于示例#2, 仅当指向 Superb 对象时，if 条件才为 true, 如果指向的是从 Superb 派生出来的对象，则 if 条件不为 true。

#### 7

Dynamic_cast 运算符只允许沿类层次结构向上转换，而 static_cast
运算符允许向上转换和向下转换。static_cast 运算符还允许枚举类型和整
型之间以及数值类型之间的转换。
