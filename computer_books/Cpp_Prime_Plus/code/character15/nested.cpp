#include "queuetp.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {

  Queuetp<string> cs(5);
  string temp;
  while (!cs.isfull()) {
    cout << "Please enter your name. You will be"
            " served in the order of arrival.\n";
    getline(cin, temp);
    cs.equeue(temp);
  }
  cout << "The queue is full. Processing begins!\n";
  while (!cs.isempty()) {
    cs.dequeue(temp);
    cout << "Now processing " << temp << "...\n";
  }

  return 0;
}
