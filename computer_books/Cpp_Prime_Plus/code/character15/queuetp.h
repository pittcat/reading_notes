#ifndef QUEUETP_H
#define QUEUETP_H

template <class Item> class Queuetp {
private:
  enum { Q_SIZE = 10 };
  class Node {
  public:
    Item item;
    Node *next;
    Node(const Item &i) : item(i), next(0) {}
  };
  Node *front;
  Node *rear;
  int items;
  const int qsize;
  Queuetp(const Queuetp &q) : qsize(0){};
  Queuetp &operator=(const Queuetp &q) { return *this; };

public:
  Queuetp(int qs = Q_SIZE);
  ~Queuetp();
  bool isempty() const { return items == 0; };
  bool isfull() const { return items == qsize; };
  int queuecoount() const { return items; }
  bool equeue(const Item &item);
  bool dequeue(Item &item);
};

// QuueueTP methods
template <class Item> Queuetp<Item>::Queuetp(int qs) : qsize(qs) {
  front = rear = 0;
  items = 0;
};

template <class Item> Queuetp<Item>::~Queuetp() {
  Node *temp;
  while (front != 0) {
    temp = front;
    front = front->next;
    delete temp;
  }
}

template <class Item> bool Queuetp<Item>::equeue(const Item &item) {
  if (isfull()) {
    return false;
  }
  Node *add = new Node(item);
  items++;
  if (front == 0) {
    front = add;
  } else {
    rear->next = add;
  }
  rear = add;
  return true;
}

template <class Item> bool Queuetp<Item>::dequeue(Item &item) {
  if (front == 0) {
    return false;
  }
  item = front->item;
  items--;
  Node *temp = front;
  front = front->next;
  delete temp;
  if (items == 0) {
    rear = 0;
  }
  return true;
};

#endif /* QUEUETP_H */
