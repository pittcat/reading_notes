#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  ifstream fin;
  fin.open("/home/pitt/CloudDrive/Mega/gitlab/reading_notes/computer_books/"
           "Cpp_Prime_Plus/code/character16/tobuy.txt");
  if (fin.is_open() == false) {
    cerr << "Can't open file. Bye.\n";
    exit(EXIT_FAILURE);
  }
  string item;
  int count = 0;
  getline(fin, item, ':');
  while (fin) {
    ++count;
    cout << count << ": " << item << endl;
    getline(fin, item, ':');
  }
  cout << "Done\n";
  fin.close();

  return 0;
}
