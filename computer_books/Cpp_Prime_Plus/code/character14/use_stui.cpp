#include "studenti.h"
#include <iostream>
using namespace std;

const int pupils = 1;
const int quizzes = 5;

int main() {
    
  Student ada[pupils]={Student("Cil Bayts",{92,94,93,95,96})};
  int i;
  
  cout << "\nStudent List:\n";
  for (i = 0; i < pupils; ++i) {
    cout << ada[i].Name() << endl;
  }
  cout << "\nResults:";
  for (i = 0; i < pupils; ++i) {
    cout << endl << ada[i];
    cout << "average: " << ada[i].Average() << endl;
  }
  cout << "Done.\n";
  return 0;
}
