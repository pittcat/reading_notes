#### 1

![image_2020-12-10-15-07-14](img/image_2020-12-10-15-07-14.png)

#### 2

```cpp
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Frabjouts {
private:
  const char *fab;

public:
  Frabjouts(const char *s = "C++") : fab(s){};
  virtual void tell()const { cout << fab<<endl; };
};

class Gloam {
private:
  int glip;
  Frabjouts fb;

public:
  Gloam(int g = 0, const char *s = "C++");
  Gloam(int g, const Frabjouts &f);
  void tell() const;
};

Gloam::Gloam(int g, const char *s) : glip(g), fb(s) {}
Gloam::Gloam(int g,const Frabjouts &f):glip(g),fb(f) {}

void Gloam::tell()const{
    fb.tell();
    cout<<"glip:"<<glip<<endl;
}


int main() {
    Gloam a;
    a.tell();
    return 0; }
```

#### 3

```cpp

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Frabjouts {
private:
  string fab;

public:
  Frabjouts(string s = "C++") : fab(s) {}
  virtual void tell() { cout << fab<<endl; }
};

class Gloam : private Frabjouts {
private:
  int glip;

public:
  Gloam(int g = 0, const char *s = "C++");
  Gloam(int g, const Frabjouts &f);
  void tell();
};

Gloam::Gloam(int g, const char *s) : glip(g), Frabjouts(s) {}
Gloam::Gloam(int g, const Frabjouts &f) : glip(g), Frabjouts(f) {}

void Gloam::tell() {
  Frabjouts::tell();
  cout << glip << endl;
}

int main(int argc, char *argv[]) {
  Gloam a(2,"cannot");
  a.tell();
  return 0;
}
```

#### 4

#### 5

#### 6

如果两条继承路线有相同的祖先,则类中将包含祖先成员的两个
拷贝。将祖先类作为虚基类可以解决这种问题。
