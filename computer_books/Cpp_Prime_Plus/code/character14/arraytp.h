#ifndef ARRAYTP_H
#define ARRAYTP_H
#include <cstdlib>
#include <iostream>

template <class T, int n> class ArrayTb {
private:
  T ar[n];

public:
  ArrayTb(){};
  explicit ArrayTb(const T &v);
  virtual T &operator[](int i);
  virtual T operator[](int i) const;
};

template <class T, int n> ArrayTb<T, n>::ArrayTb(const T &v) {
  for (int i = 0; i < n; ++i) {
    ar[i] = v;
  }
}

template <class T, int n> T &ArrayTb<T, n>::operator[](int i) {

  if (i < 0 || i >= n) {
    std::cerr << "Error in array limits: " << i << " is out of range\n";
    std::exit(EXIT_FAILURE);
  }
  return ar[i];
}

template <class T, int n> T ArrayTb<T, n>::operator[](int i) const {
  if (i < 0 || i >= n) {
    std::cerr << "Error in array limits: " << i << " is out of range\n";
    std::exit(EXIT_FAILURE);
  }
  return ar[i];
}

#endif /* ARRAYTP_H */
