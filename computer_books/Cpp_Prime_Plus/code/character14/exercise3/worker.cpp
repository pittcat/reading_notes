#include "worker.h"
#include <cstdio>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

void Worker::Set() {
  cout << "Enter worker's name: ";
  getline(cin, fullname);
  cout << "Enter worker's ID:";
  std::cin >> id;
  while (cin.get() != '\n') {
    continue;
  }
}

void Worker::Show() const {
  cout << "Name: " << fullname << endl;
  cout << "Employee ID:" << id << "\n";
}
