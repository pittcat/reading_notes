#ifndef PERSON_H
#define PERSON_H

#include <string>
class Person {
private:
  std::string firstname;
  std::string lastname;

public:
  Person(const char *fn = "none", const char *ln = "none")
      : firstname(fn), lastname(ln){};
  virtual ~Person(){};
  virtual void Show() const;
  virtual void Set();
};

#endif /* PERSON_H */
