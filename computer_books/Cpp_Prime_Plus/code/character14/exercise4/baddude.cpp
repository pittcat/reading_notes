#include "baddude.h"

void Baddude::Show() const {
  Gunslinger::Show();
  Pokerplayer::Show();
}

void Baddude::Set() { Gunslinger::Set(); }
