#ifndef POKERPLAYER_H
#define POKERPLAYER_H

#include "person.h"

class Pokerplayer : virtual public Person {
private:
public:
  Pokerplayer(const char *fn = "none", const char *ln = "none")
      : Person(fn, ln) {}
  int Draw() const;
};

#endif /* POKERPLAYER_H */
