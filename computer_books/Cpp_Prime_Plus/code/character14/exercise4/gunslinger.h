#ifndef GUNSLINGER_H
#define GUNSLINGER_H

#include "person.h"

class Gunslinger : virtual public Person {
private:
  double drawtime;
  int notches;

public:
  Gunslinger(const char *fn = "none", const char *ln = "none", double d = 0.0,
             int n = 0)
      : Person(fn, ln), drawtime(d), notches(n){};

  double Draw() const { return drawtime; }
  void Show() const;
  void Set();
  virtual ~Gunslinger();
};

#endif /* GUNSLINGER_H */
