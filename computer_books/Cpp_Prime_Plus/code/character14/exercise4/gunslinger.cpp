#include "gunslinger.h"
#include <iostream>

void Gunslinger::Show() const {
  Person::Show();
  std::cout << "Draw: " << drawtime << std::endl;
  std::cout << "Notches: " << notches << std::endl;
}

void Gunslinger::Set() {
  Person::Set();
  std::cout << "Enter Drawtime: ";
  
  std::cin >> drawtime;
  std::cout << "Enter Notches: ";
  std::cin >> notches;
}
