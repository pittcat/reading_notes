#ifndef BADDUDE_H
#define BADDUDE_H

#include "gunslinger.h"
#include "pokerplayer.h"

class Baddude : public Pokerplayer, public Gunslinger {

public:
  double Gdraw() const { return Gunslinger::Draw(); };
  double Cdraw() const { return Pokerplayer::Draw(); };
  void Show() const;
  void Set();
};

#endif /* BADDUDE_H */
