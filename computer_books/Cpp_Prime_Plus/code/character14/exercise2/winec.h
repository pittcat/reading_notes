#ifndef WINEC_H
#define WINEC_H

#include <string>
#include <valarray>

template <class T1, class T2> class Pair {
private:
  T1 a;
  T2 b;

public:
  T1 &first();
  T2 &second();
  T1 first() const { return a; };
  T2 second() const { return b; };
  Pair(const T1 &aval, const T2 &bval) : a(aval), b(bval){};
  Pair(const Pair<T1, T2> &p);
  Pair(){};
};

template <class T1, class T2> T1 &Pair<T1, T2>::first() { return a; };

template <class T1, class T2> T2 &Pair<T1, T2>::second() { return b; };

typedef std::valarray<int> ArrayInt;
typedef Pair<ArrayInt, ArrayInt> PairArray;

template <typename T1, typename T2> Pair<T1, T2>::Pair(const Pair<T1, T2> &p) {
  a = p.a;
  b = p.b;
}

class Wine : private PairArray, private std::string {

public:
  Wine(const char *l, int y, const int yr[], const int bot[]);
  Wine(const char *l, int y);
  void GetBottles();
  std::string &Label()const {return (std::string &)(*this);};
  void Show()const;
  int sum();

private:
  int year;
};

#endif /* WINEC_H */
