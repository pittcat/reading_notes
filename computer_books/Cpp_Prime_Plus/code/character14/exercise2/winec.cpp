#include "winec.h"
#include <iostream>
#include <string>

using namespace ::std;

Wine::Wine(const char *l, int y, const int yr[], const int bot[])
    : std::string(l), year(y), PairArray(ArrayInt(yr, y), ArrayInt(bot, y)) {}

Wine::Wine(const char *l, int y)
    : std::string(l), year(y), PairArray(ArrayInt(y), ArrayInt(y)) {}

void Wine::GetBottles() {
  int y, b;
  cout << "Enter " << Label() << " data for " << year << " year(s)" << endl;
  for (int i = 0; i < year; ++i) {
    cout << "Enter year:";
    std::cin >> y;
    PairArray::first()[i] = y;
    cout << "Enter bottles for that year: ";
    std::cin >> b;
    PairArray::second()[i] = b;
  }
}

void Wine::Show() const {
  cout << "Wine: " << Label() << endl;
  cout << "Year: "
       << "   Bottles" << endl;
  for (int i = 0; i < year; ++i) {
    cout << PairArray::second()[i] << "     ";
    cout << PairArray::first()[i] << endl;
  }
}

int Wine::sum() { return PairArray::second().sum(); }
