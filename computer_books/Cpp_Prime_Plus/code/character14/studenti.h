#ifndef STUDENTI_H
#define STUDENTI_H

#include <iostream>
#include <istream>
#include <ostream>
#include <string>
#include <valarray>
using namespace ::std;

class Student : private string, private valarray<double> {
private:
  typedef valarray<double> ArrayDb;
  ostream &arr_out(ostream &os) const;

public:
  Student() : string("Null Student"), ArrayDb(){};
  explicit Student(const string &s) : string(s), ArrayDb(){};
  explicit Student(int n) : string("Nully"), ArrayDb(){};
  Student(const string &s, int n) : string(s), ArrayDb(n){};
  Student(const string &s, const ArrayDb &a) : string(s), ArrayDb(a){};
  Student(const char *str, const double *pd, int n)
      : string(str), ArrayDb(pd, n){};
  virtual ~Student(){};
  double Average() const;
  double &operator[](int i);
  double operator[](int i) const;
  const string &Name() const;
  // friends
  // input
  friend istream &operator>>(istream &is, Student &stu);
  friend istream &getline(istream &is, Student &stu);
  // output
  friend ostream &operator<<(ostream &os, const Student &stu);
};

#endif /* STUDENTI_H */
