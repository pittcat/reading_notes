#include "winec.h"
#include <iostream>

using std::cout;
using std::endl;

Wine::Wine(const char *l, int y, const int yr[], const int bot[])
    : name(l), year(y), info(ArrayInt(yr, y), ArrayInt(bot, y)) {}

Wine::Wine(const char *l, int y)
    : name(l), year(y), info(ArrayInt(y), ArrayInt(y)) {}

void Wine::GetBottles() {
  int y, b;
  std::cout << "Enter " << name << " data for " << year << " year(s)"
            << std::endl;
  for (int i = 0; i < year; ++i) {
    cout << "Enter year:";
    std::cin >> y;
    info.first()[i] = y;
    cout << "Enter bottles for that year:";
    std::cin >> b;
    info.second()[i] = b;
  }
}

int Wine::sum() { return info.second().sum(); }

std::string &Wine::Label() { return name; }

void Wine::Show() {
  cout << "Year "
       << " Bottles" << endl;
  for (int i = 0; i < year; ++i) {
    cout << info.second()[i] << "   ";
    cout << info.first()[i] << endl;
  }
}
