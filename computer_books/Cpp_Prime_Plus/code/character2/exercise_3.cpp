#include <cstring>
#include <iostream>
#include <vector>

void blind(void);

void how(void);

using namespace std;

int main() {
  blind();
  blind();
  how();
  how();
  return 0;
}

void blind(void) { cout << "Three blind mice" << endl; }

void how(void) { cout << "See how they run" << endl; }
