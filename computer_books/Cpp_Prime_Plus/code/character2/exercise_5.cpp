#include <algorithm>
#include <cstring>
#include <iostream>
#include <vector>

using namespace std;

int main() {
  double celsius, fahrenheit;
  cout << "Please input a Celsius value:";
  cin >> celsius;
  fahrenheit = 1.8 * celsius + 32.0;
  cout << celsius << " degrees Celsius is " << fahrenheit
       << " degrees Fahrenheit";
  return 0;
}
