#include <cstring>
#include <iostream>
#include <vector>

using namespace std;

void lyear2disdance(void);

int main() { return 0; }

void lyear2disdance(void) {
  double lyear, disdance;
  cout << "Enter the number of light years:";
  cin >> lyear;
  disdance = lyear * 63240;
  cout << lyear << "light years = " << disdance << " astronomical units.";
}
