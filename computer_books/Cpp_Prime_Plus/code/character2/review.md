#### 1

函数

#### 2

这将导致在最终的编译之前，使用 iostream 文件的内容替换该编译指令。

#### 3

它使得程序可以使用 std 名称空间中的定义

#### 4

```cpp
cout << "Hello , world\n";
```

### 5 6 7 8

```cpp
int cheeses;
cheeses=32;
cin >> cheeses;
cout << "We have " << cheeses << " varieties of cheese,";
```

###3 9

- 调用函数 froop( )时，应提供一个参数，该参数的类型为 double，而该函数将返回一个 int 值。
- 函数 rattle( )接受一个 int 参数且没有返回值。
- 函数 prune( )不接受任何参数且返回一个 int 值。

#### 10

当函数的返回类型为 void 时，不用在函数中使用 return。然而，如果不提供返回值，则可以使用它：
return;

#### 11

```cpp

#include <cstring>
#include <iostream>
#include <vector>

using namespace std;
using std::cout;

int main() {
  cout << "Please enter your PIN:";
  cout << endl;
  std::cout << "Please enter PIN:" << endl;
  return 0;
}
```
