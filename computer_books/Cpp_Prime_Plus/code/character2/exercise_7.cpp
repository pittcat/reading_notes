#include <cstring>
#include <iostream>
#include <vector>

using namespace std;

void show_time(int, int);
int main() {
  int h, m;
  cout << "Enter the number of hours:";
  cin >> h;
  cout << "Enter the number of minutes:";
  cin >> m;
  show_time(h, m);

  return 0;
}

void show_time(int hours, int minutes) {
  cout << "Time: " << hours << ":" << minutes;
}
