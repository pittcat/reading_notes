#include <cmath>
#include <cstring>
#include <iostream>
#include <vector>

using namespace std;

int main() {
  double area;
  cout << "Enter the floor area, in square feet, of your home:";
  cin >> area;
  double side;
  side = sqrt(area);
  cout << "That's the equivalent of square " << side << " feet to the side."
       << endl;
  cout << "How fascinnating!" << endl;
  return 0;
}
