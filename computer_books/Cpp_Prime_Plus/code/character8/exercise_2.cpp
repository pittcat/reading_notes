#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct CandyBar {
  string name;
  float weight;
  int calory;
};

void set_candy(CandyBar &n) {
  cout << "name:";
  cin >> n.name;
  cout << "weight:";
  cin >> n.weight;
  cout << "calory:";
  cin >> n.calory;
}

void display_candy(const CandyBar &n) {
  cout << "name: " << n.name << " weight:" << n.weight
       << " height:" << n.calory;
}

int main() {
  CandyBar n;
  set_candy(n);
  display_candy(n);
  return 0;
}
