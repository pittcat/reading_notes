#include <cstring>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

template <typename T> T maxn(T *, int);
template <> char *maxn<char *>(char **, int);
template <typename T> void show(T ar[], int);

int main() {
  double d_ar[4] = {1.1, 1.34, 1.00, 3.44};
  int i_ar[6] = {1, 2, 4, 5, 5, 6};
  const char *s_ar[5] = {"lod", "cat", "pitt", "dragon", "cat"};
  show(d_ar, 4);
  cout << maxn(d_ar, 4) << endl;
  show(i_ar, 6);
  cout << maxn(i_ar, 6) << endl;
  show(s_ar, 5);
  cout << maxn(s_ar, 5);

  return 0;
}

template <class T> T maxn(T arr[], int n) {
  T max = arr[0];
  for (int i = 1; i < n; ++i) {
    if (max < arr[i]) {
      max = arr[i];
    }
  }
  return max;
}
template <> char *maxn<char *>(char **arr, int n) {
  int max_len = strlen(arr[0]);
  int tmp_len;
  int max_idx = 0;

  int i;
  for (i = 0; i < n; ++i) {
    tmp_len = strlen(arr[i]);
    if (tmp_len > max_len) {
      max_len = tmp_len;
      max_idx = i;
    }
  }

  return arr[max_idx];
}

template <class T> void show(T a[], int n) {
  for (int i = 0; i < n; ++i) {
    cout << a[i] << " ";
  }
  cout << endl;
};
