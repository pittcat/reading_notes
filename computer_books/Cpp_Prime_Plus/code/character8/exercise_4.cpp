#include <cstring>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct stringy {
  char *str;
  int ct;
};

void set(stringy &n, const char *c) {
  n.str = new char[strlen(c) + 1];
  strcpy(n.str, c);
  n.ct = strlen(c);
}

void show(const stringy stry) { cout << stry.str; }
void show(const stringy stry, int n) {

  for (int i = 0; i < n; ++i) {

    cout << stry.str << endl;
  }
}

void show(const string str) { cout << str << endl; }

void show(const string str, int n) {
  for (int i = 0; i < n; ++i) {
    cout << str << endl;
  }
}

int main() {
  stringy beany;
  char testing[] = "Reality isn't what it used to be.";
  set(beany, testing);
  show(beany);
  show(beany, 2);
  testing[0] = 'D';
  testing[1] = 'u';
  show(testing);
  show(testing, 3);
  show("Done!");

  return 0;
}
