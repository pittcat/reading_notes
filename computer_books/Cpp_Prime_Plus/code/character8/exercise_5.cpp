#include <iostream>
#include <string>
#include <vector>

using namespace std;

template <class T> T max5(T a[]) {
  T max = a[0];
  for (int i = 0; i < 5; ++i) {
    if (max < a[i]) {
      max = a[i];
    }
  }
  return max;
};
int main() {
  double d_ar[5] = {1.0, 3.4, 4.4, 5, 0.6};
  int i_ar[5] = {1, 4, 5, 5, 6};
  cout << max5(d_ar) << endl;
  cout << max5(i_ar) << endl;
  return 0;
}
