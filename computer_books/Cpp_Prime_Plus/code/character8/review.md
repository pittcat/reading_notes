#### 1

只有一行代码的小型、非递归函数适合作为内联函数.

#### 2
a,c
```cpp
void song(const char * name="O.My Papa",int times=1);
```

b．没有。只有原型包含默认值的信息。

#### 3

```cpp
void iquote(int n) { cout << "\"" << n << "\""; }

void iquote(double x) { cout << '"' << x << '"'; }

void iquote(const char *str) { cout << "\"" << str << "\""; }
```

#### 4

```cpp
#include <iostream>
#include <string>
#include <vector>

using namespace std;
struct box {
  char maker[40];
  float height;
  float width;
  float length;
  float volume;
};

void a(box &n1);
void b(box &n2);

int main() { return 0; }

void a(box &n) {
  cout << "maker:" << n.maker << endl;
  cout << "height:" << n.height << endl;
  cout << "width:" << n.length << endl;
  cout << "volume:" << n.volume << endl;
}

void b(box &n2) { n2.volume = n2.height * n2.length * n2.width; }
```

#### 5

```cpp
void fill(array<double, Seasons> &pa);
void show(array<double, Seasons> &da);
void fill(array<double, Seasons> *pa) {
  for (int i = 0; i < Seasons; ++i) {
    cout << "Enter " << Snames[i] << " expenses: ";
    cin >> pa[i];
  }
}
```

函数show( )的调用不需要修改。

#### 6

a. 
```cpp
double mass(double d,double v=1.0);
```

b. 
```cpp
void repeat(int times,const char *str);
void repeat(const char *str);
```

c. 
```cpp
int average(int a,int b);
double average(double x,double y);
```

d.  
不能这样做，因为两个版本的特征标将相同。

#### 7

```cpp
template <class T> T max(T t1, T t2) { return t1 > t2 ? t1 : t2; }
```
#### 8

```cpp
template <> box max(box b1,box b2){
    return b1.volume > b2.volume ? b1 : b2;
}
```
#### 9
v1的类型为float，v2的类型为float &，v3的类型为float &，v4的类型为int，v5的类型为double。字面值2.0的类型为double，因此表达式2.0 *m的类型为double
