#include <cctype>
#include <iostream>
#include <string>
#include <vector>

using namespace std;
const int str_size = 100;
void upper_string(string &str) {
  for (string::iterator it = str.begin(); it != str.end(); it++) {
    *it = toupper(*it);
  }
}

int main() {
  string input;
  cout << "Enter a string (q to quit):";
  getline(cin, input);
  while (input.compare("q") != 0) {
    upper_string(input);
    cout << input << endl;
    cout << "Enter a string (q to quit):";
    getline(cin, input);
  }
  cout << "Bye.";

  return 0;
}
