#include <iostream>
#include <string>
#include <vector>

using namespace std;

void print_str(const char *str);
void print_nstr(const char *str, int n);

int main() {
  const char *s = "abc";
  print_str(s);
  print_nstr(s, 3);
  return 0;
}

void print_str(char *str) {
  if (*str) {
    cout << str << endl;
  }
}

void print_nstr(char *str, int n) {
  for (int i = 0; i < n; ++i) {
    cout << str << endl;
  }
}
