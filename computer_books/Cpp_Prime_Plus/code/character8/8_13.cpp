#include <iostream>
#include <string>
#include <vector>

using namespace std;
template <typename T> void Swap(T &a, T &b);
struct job {
  char name[40];
  double salary;
  int floor;
};

template <> void Swap<job>(job &j1, job &j2);
void Show(job &j);

int main() { return 0; }

template <typename T> void Swap(T &a, T &b) {
  T temp;
  temp = a;
  a = b;
  b = temp;
}

template <> void Swap<job>(job &j1, job &j2) {
  double t1;
  int t2;
  t1 = j1.salary;
  j1.salary = j2.salary;
  t2 = j1.floor;
  j1.floor = j2.floor;
  j2.floor = t2;
}

void Show(job &j) {
  cout << j.name << ": $" << j.salary << " on floor " << j.floor << endl;
}
