#### 1

类是用户定义的类型的定义。类声明指定了数据将如何存储，同时指定了用来访问和操纵这些数据的方法（类成员函数）。

#### 2

类表示人们可以类方法的公有接口对类对象执行的操作，这是抽象。类的数据成员可以是私有的（默认值）, 这意味着只能通过成员函数来访问这些数据，这是数据隐藏。实现的具体细节（如数据表示和方法的代码）都是隐藏的，这是封装。

#### 3

类定义了一种类型，包括如何使用它。对象是一个变量或其他数据对象（如由 new 生成的）, 并根据类定义被创建和使用。类和对象之间的关系同标准类型与其变量之间的关系相同。

#### 4

如果创建给定类的多个对象，则每个对象都有其自己的数据内存空间；但所有的对象都使用同一组成员函数（通常，方法是公有的，而数据是私有的，但这只是策略方面的问题，而不是对类的要求）。

#### 5

```cpp
#include <string>

using namespace std;
class BankAccount {
private:
  string name;
  string account;
  double balance;

public:
  BankAccount(const string &client, const string &num, double balance = 0.0);
  void show(void) const;
  void deposit(double cash);
  void withdraw(double cash);
};

```

#### 6

在创建类对象或显式调用构造函数时，类的构造函数都将被调用。当对象过期时，类的析构函数将被调用。

#### 7

```cpp
BankAccount::BankAccount(const string &name, const string &num, double bal) {
  cout << endl;
}
```

#### 8

默认构造函数是没有参数或所有参数都有默认值的构造函数。拥有默认构造函数后，可以声明对象，而不初始化它，即使已经定义了初始化构造函数。它还使得能够声明数组。

#### 9

#### 10

this 指针是类方法可以使用的指针，它指向用于调用方法的对象。因此，this 是对象的地址，\*this 是对象本身。
