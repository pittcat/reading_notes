#include <iostream>
#include <string>
#include <vector>

using namespace std;

class BankAccount {
private:
  string name;
  string account;
  double balance;

public:
  BankAccount() {
    name = "";
    account = "";
    balance = 0.0;
  };
  BankAccount(const string &client, const string &num, double bal = 0.0);
  void show(void) const;
  void deposit(double cash);
  void withdraw(double cash);
};

BankAccount::BankAccount(const string &client, const string &num, double bal) {
  name = client;
  account = num;
  balance = bal;
}

void BankAccount::show() const {
  cout << "Bank Info:" << endl;
  cout << "name: " << name << endl;
  cout << "account: " << account << endl;
  cout << "balance: " << balance << endl;
}
void BankAccount::deposit(double cash) { balance += cash; }

void BankAccount::withdraw(double cash) { balance -= cash; }

int main() {
  BankAccount A;
  BankAccount B{"cat", "123", 13.4};
  B.show();
  A=B;
  A.show();
  A.deposit(2);
  A.show();
  A.withdraw(1);
  A.show();
  return 0;
}
