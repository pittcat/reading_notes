#include <iostream>
#include <string>
#include <vector>

using namespace std;

const int MAX = 10;
struct people {
  string name;
  int age;
};

typedef struct people Item;
class List {
private:
  Item items[MAX];
  int count;

public:
  List();
  bool isempty() const;
  bool isfull() const;
  int itemcount() const;
  void additem(const Item &item);
  void visit(void (*pf)(Item &));
};

List::List() { count = 0; }
bool List::isempty() const { return count == 0; }

bool List::isfull() const { return count == MAX; }

int List::itemcount() const { return count; }

void List::additem(const Item &item) {
  if (isfull()) {
    cout << "full already" << endl;
  } else {
    items[++count] = item;
  }
}

void List::visit(void (*pf)(Item &)) {
  for (int i = 0; i < count; ++i)
    (*pf)(items[i]);
}

void addage(Item &item);
int main() {

  List l;
  Item i = {"pittcat", 20};
  l.additem(i);
  int n = l.itemcount();
  cout << n << " items in list" << endl;
  l.visit(addage);
  return 0;
}

void addage(Item &item) { item.age += 1; }
