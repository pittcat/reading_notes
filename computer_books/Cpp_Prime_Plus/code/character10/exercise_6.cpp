#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Move {
private:
  double x;
  double y;

public:
  Move(double a = 0, double b = 0);
  void showmove() const;
  Move add(const Move &m) const;
  void reset(double a = 0, double b = 0);
};

Move::Move(double a, double b) {
  x = a;
  y = b;
}

void Move::showmove() const {
  cout << "x:" << x << endl;
  cout << "y:" << y << endl;
}

Move Move::add(const Move &m) const {
  Move n = Move(x + m.x, y + m.y);
  return n;
}

void Move::reset(double a, double b) {
  x = a;
  y = b;
}

int main() {

  Move m1;
  m1.showmove();
  m1.reset(2, 3);
  m1.showmove();
  Move m2 = Move(3, 1);
  m2.showmove();
  Move m3;
  m3 = m2.add(m1);
  m3.showmove();
  return 0;
}

