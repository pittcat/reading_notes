#include <cstring>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class prog {
  static const int LEN = 20;
  char name[LEN];
  int CI;

public:
  prog() {
    strcpy(name, "Ploga");
    CI = 50;
  };
  void showplorg() const;
  void change_plorg_ci(int n);
};

void prog::showplorg() const {
  cout << "prog name:" << name << endl;
  cout << "prog ci:" << CI << endl;
}
void prog::change_plorg_ci(int n) { CI = n; }
int main() {
  prog a;
  a.showplorg();
  a.change_plorg_ci(3);

  a.showplorg();
  return 0;
}
