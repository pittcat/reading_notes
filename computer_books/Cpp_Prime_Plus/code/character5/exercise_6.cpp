#include <iostream>
#include <string>
#include <vector>

using namespace std;
const int year = 3;
const int month = 12;

int main() {
  double year_sum, all_sum;
  all_sum = 0.0;
  double sale[year][month];
  string Mon[month] = {"Jan", "Feb", "Mar",  "Apr", "May", "Jun",
                       "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};
  for (int i = 1; i < year + 1; ++i) {
    cout << "year " << i << " info";
    year_sum = 0.0;
    for (int j = 0; j < month; ++j) {
      cout << Mon[j] << " sales :";
      cin >> Mon[i][j];
      year_sum += Mon[i][j];
      all_sum += Mon[i][j];
    }
    cout << "Year " << i << " sales " << year_sum;
  }
  cout << "Three Years Sales :" << all_sum;
  return 0;
}
