#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main() {
    int min,max,i,sum=0;
    std::cout << "Please input min number:" << std::endl;
    cin>>min;
    std::cout << "Please input max number:" << std::endl;
    cin>>max;
    for (i = min; i < max+1; ++i) {
        sum+=i;
    }
    std::cout << "Sum:"<<sum << std::endl;
    
    return 0;
}
