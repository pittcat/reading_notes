#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  int ch;
  int count = 0;
  while ((ch = cin.get()) != EOF) {
    cout.put(char(ch));
    ++count;
  }
  cout << endl << count << " characters reads\n";
  return 0;
}
