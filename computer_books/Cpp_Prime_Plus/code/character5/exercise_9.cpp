#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  string cur;
  int count = 0;
  cout << "Enter words (to stop, type word done):";
  cin >> cur;
  while (cur != "done") {
    count++;
    cin >> cur;
  }
  cout << "You entered a total of " << count << " words.";
  return 0;
}
