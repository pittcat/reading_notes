#include <cstring>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

const int Char_Max = 100;

int main() {
  char str[Char_Max];
  int count = 0;
  cout << "Enter words (to stop, type word done):";
  cin >> str;
  while (strcmp(str, "done")) {
    count++;
    cin >> str;
  }
  cout << "You entered a total of " << count << " words.";

  return 0;
}
