#include <array>
#include <iostream>
#include <string>
#include <vector>

using namespace std;
const int num = 12;

int main() {
  string Mon[num] = {"Jan", "Feb", "Mar",  "Apr", "May", "Jun",
                     "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};
  array<double, num> sales;
  double sum = 0.0;
  cout << "Please input sales every month\n";
  for (int i = 0; i < num; ++i) {
    cout << Mon[i] << " sales:";
    cin >> sales[i];
    sum += sales[i];
  }
  cout << "Sum = " << sum;

  return 0;
}
