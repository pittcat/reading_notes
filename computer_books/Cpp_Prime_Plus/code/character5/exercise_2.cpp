#include <array>
#include <iostream>
#include <string>
#include <vector>

using namespace std;
const int Arsize = 100;
int main() {
  array<long double, Arsize> factorials;
  factorials[1] = factorials[0] = 1LL;
  for (int i = 2; i < Arsize; ++i) {
    factorials[i] = i * factorials[i - 1];
  }
  cout << Arsize << "! = " << factorials[Arsize - 1];
  return 0;
}
