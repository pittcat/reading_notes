#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  char ch;
  int count = 0;
  cout << "Enter characters; enter # to quit:\n";
  cin.get(ch);
  while (ch != '#') {
    cout << ch;
    ++count;
    cin.get();
  }
  cout << endl << count << " characters read\n";

  return 0;
}
