#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  int a = 20;
  int b = 20;
  cout << "a = " << a << ": b = " << b << endl;
  cout << "a++ = " << a++ << ": ++b = " << ++b << "\n";
  cout << "a = " << a << ": b = " << b << "\n";
  return 0;
}
