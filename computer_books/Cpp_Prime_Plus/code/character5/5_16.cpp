#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  char ch;
  int count;
  cout << "Enter characters;enter # to quit:\n";
  cin >> ch;
  while (ch != '#') {
    cout << ch;
    ++count;
    cin >> ch;
  }
  cout << endl << count << " characters read\n";
  return 0;
}
