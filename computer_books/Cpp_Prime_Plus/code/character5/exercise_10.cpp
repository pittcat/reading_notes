#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  int num, cur;
  cout << "Enter number of rows:";
  cin >> num;
  cout << endl;
  for (int i = 0; i < num; ++i) {
    cur = i + 1;
    for (int j = 0; j < num - cur; ++j) {
      cout << ".";
    }
    for (int k = 0; k < cur; ++k) {
      cout << "*";
    }
    cout << endl;
  }

  return 0;
}
