#include <iostream>
#include <string>
#include <vector>

using namespace std;
struct car {
  string maker;
  int year;
};

int main() {
  int n;
  cout << "How many cars do you wish to catalog ?";
  cin >> n;
  cin.get();
  car *p_car_info = new car[n];
  for (int i = 0; i < n; ++i) {
    cout << "Car #" << i + 1 << ":" << endl;
    cout << "Please enter the make:";
    getline(cin, p_car_info[i].maker);
    cout << "Please enter the year make:";
    cin >> p_car_info[i].year;
    cin.get();
  }
  cout << "Here is your collection:\n";
  for (int i = 0; i < n; ++i) {
    cout << p_car_info[i].year;
    cout << " ";
    cout << p_car_info[i].maker;
    cout << endl;
  }
  return 0;
}
