#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  int n, sum = 0;
  cout << "Please input a number:";
  cin >> n;
  for (int i = n; i > 0; i--) {
    sum += i;
  }
  cout<<"sum = "<<sum;
  return 0;
}
