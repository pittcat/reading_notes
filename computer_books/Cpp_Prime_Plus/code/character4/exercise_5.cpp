#include <iostream>
#include <string>
#include <vector>

using namespace std;
struct candy {
  string kind;
  double weight;
  int calorie;
};
int main() {
  candy snack{"Mocha Munch", 2.5, 350};
  cout << "Name:" << snack.kind << endl;
  cout << "Weight:" << snack.weight << endl;
  cout << "Calories:" << snack.calorie;
  return 0;
}
