#include <iostream>
#include <string>
#include <vector>

using namespace std;
struct pizza {
  string company;
  double diameter;
  double weight;
};

int main() {
  pizza pz;
  cout << "Please input pizza company name:";
  getline(cin, pz.company);
  cout << "Please input pizza diameter:";
  cin >> pz.diameter;
  cout << "Please input pizza weight:";
  cin >> pz.weight;
  cout << "Pizza information:" << pz.company << " " << pz.diameter << " "
       << pz.weight;
  return 0;
}
