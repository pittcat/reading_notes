#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  string fname, lname;
  cout << "Enter your first name:";
  getline(cin, fname);
  cout << "Enter your last name:";
  getline(cin, lname);
  cout << "Here's the information in single string:";
  cout << lname << ", " << fname;
  return 0;
}
