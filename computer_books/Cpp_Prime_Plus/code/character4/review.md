#### 1
```cpp
char actor[30];
short betsie[100];
float chunk[13];
long double dipsea[64];
```

#### 2

```cpp
#include <array>

using namespace std;

int main() {
    array<char, 30> actor;
    array<short, 100> betsie;
    array<float, 13> chuck;
    array<long double, 64> dipsea;
    

    return 0;
}
```

#### 3
```cpp
array<int, 5> n ={1,3,5,7,9};
```

#### 4
```cpp
int even;
array<int, 5> n ={1,3,5,7,9};
even=n[0]+n[4];
```

#### 5
```cpp
float arr[3]{1, 2, 3};
cout << arr[1];
```
#### 6
```cpp
char s_arr[20]="cheeseburger";
```

#### 7
```cpp
string str="Waldorf Salad";
```

#### 8
```cpp
struct fish{
    string kind;
    int weight;
    double length;
};
```

#### 9
```cpp
#include <iostream>
#include <vector>
#include <string>

using namespace std;
struct fish{
    string kind;
    int weight;
    double length;
};

int main() {
    fish test { "bigfish",12,1.2};
    return 0;
}
```

#### 10
```cpp
enum Response {No,Yes,Maybe};
```

#### 11
```cpp
    double ted=1.0;
    double * p_tes=&ted;
    cout<<*p_tes;
```

#### 12
```cpp
float treacle[10];
for (int i = 0; i < 10; ++i) {
    treacle[i]=i;
}
float *p_treacle=treacle;
cout <<p_treacle[0]<<endl;
cout<<p_treacle[9];
```

#### 13

```cpp
int n;
cout << "Please input a number:";
cin >> n;
int *p_i = new int[n];
vector<int> p_v(n);
```

#### 14
是的，它是有效的。表达式“home of the jolly bytes”是一个字符串常量，因此，它将判定为字符串开始的地址。cout对象将char地址解释为打印字符串，但类型转换(int *)将地址转换为int指针，然后作为地址被打印。总之，该语句打印字符串的地址，只要int类型足够宽，能够存储该地址。

#### 15

```cpp

struct fish {
  string kind;
  int weight;
  double length;
};
fish *pole = new fish;
cout << "Enter kind fo fish:";
cin >> pole->kind;
```


#### 16

使用```cin >>address```将使得程序跳过空白，直到找到非空白字符为止。然后它将读取字符，直到再次遇到空白为止。因此，它将跳过数字输入后的换行符，从而避免这种问题。另一方面，它只读取一个单词，而不是整行。

#### 17

```cpp
#include <iostream>
#include <array>
#include <string>
#include <vector>

using namespace std;

const int Str_num{10};

int main() {
  std::vector<std::string> vstr(Str_num);
  std::array<std::string, Str_num> astr;
  return 0;
}
```


