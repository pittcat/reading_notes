#include <array>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  double sum, average;
  array<double, 3> arr;
  sum = 0;
  for (int i = 0; i < 3; ++i) {
    cout << "Consuming Time:";
    cin >> arr[i];
    sum += arr[i];
  }
  cout << "Sum: " << sum << endl;
  average = sum / 3;
  cout << "Average: " << average;

  return 0;
}
