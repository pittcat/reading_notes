#include <iostream>
#include <string>
#include <vector>

using namespace std;
struct candy {
  string kind;
  double weight;
  int calorie;
};

int main(int argc, char *argv[]) {
  candy Candybar[3]{{"Mocha Munch 1", 2.5, 350},
                    {"Mocha Munch 2", 2.335, 50},
                    {"Mocha Munch 3", 2.56, 30}};
  for (int i = 0; i < 3; ++i) {
    cout << "Candy " << i << Candybar[i].kind << " " << Candybar[i].weight
         << " " << Candybar[i].calorie;
    cout << endl;
  }

  return 0;
}
