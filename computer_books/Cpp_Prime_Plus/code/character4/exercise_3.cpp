#include <cstring>
#include <iostream>
#include <vector>

using namespace std;
const int N_size = 30;

int main() {
  char fname[N_size], lname[N_size];
  cout << "Enter your first name:";
  cin.getline(fname, N_size);
  cout << "Enter your last name:";
  cin.getline(lname, N_size);
  cout << "Here's the information in single string:";
  cout << lname << ", " << fname;

  return 0;
}
