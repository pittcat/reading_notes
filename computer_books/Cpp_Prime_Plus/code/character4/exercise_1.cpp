#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  string f_name, l_name;
  char choice;
  int age;
  cout << "What is your first name ?";
  getline(cin, f_name);
  cout << "What is your last name ?";
  getline(cin, l_name);
  cout << "What letter grade do you deserve ?";
  cin >> choice;
  cout << "What is your age ?";
  cin >> age;
  cout << "Name:" << l_name << ", " << f_name << endl;
  cout << "Grade:" << choice;
  cout << "Age:" << age;
  return 0;
}
