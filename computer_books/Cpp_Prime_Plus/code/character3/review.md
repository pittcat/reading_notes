#### 1 
有多种整型类型，可以根据特定需求选择最适合的类型。例如，可以使用short来存储空格，使用long来确保存储容量，也可以寻找可提高特定计算的速度的类型。


#### 2

```cpp
short n1=80;
unsigned int n2=42110;
long long n3=3E+9;
```

#### 3
C++没有提供自动防止超出整型限制的功能，可以使用头文件climits来确定限制情况。

#### 4


#### 5 
这两条语句并不真正等价，虽然对于某些系统来说，它们是等效的。最重要的是，只有在使用ASCII码的系统上，第一条语句才将得分设置为字母A，而第二条语句还可用于使用其他编码的系统。其次，65是一个int常量，而‘A’是一个char常量。

#### 6

```cpp
  char c = 88;
  cout << c << endl;
  cout.put(char(88));
  cout << char(88) << endl;
  cout << (char)88 << endl;
```

#### 7

这个问题的答案取决于这两个类型的长度。如果long为4个字节，则没有损失。因为最大的long值将是20亿，即有10位数。由于double提供了至少13位有效数字，因而不需要进行任何舍入。long long类型可提供19位有效数字，超过了double保证的13位有效数字。

##### 8
74, 4,0,4.5,3

#### 9

```cpp
double x1,x2;
int res1=int(x1)+int(x2);
int res2=int(x1+x2);
```

#### 10
a.int
b.float
c.char
d.char32_t
e.double
