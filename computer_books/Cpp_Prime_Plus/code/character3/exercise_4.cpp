#include <cstring>
#include <iostream>
#include <vector>

using namespace std;

int main() {
  long long seconds, c_second;
  cout << "Enter the number of seconds:";
  cin >> seconds;
  c_second = seconds;
  int days, hours, minutes;
  days = (int)seconds / 86400;
  seconds %= 86400;
  hours = (int)seconds / 3600;
  seconds %= 3600;
  minutes = (int)seconds / 60;
  seconds %= 60;
  cout << c_second << " seconds = " << days << " days, " << hours << " hours, ";
  cout << minutes << " minutes, " << seconds << " seconds";
  return 0;
}
