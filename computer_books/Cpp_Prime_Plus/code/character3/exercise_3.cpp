#include <cstring>
#include <iostream>
#include <vector>

using namespace std;

int main() {
  double degrees, minutes, seconds, res;
  cout << "Enter a lattitude in degrees, minutes , and seconds:";
  cout << "First, enter the degrees:";
  cin >> degrees;
  cout << "Next, enter the seconds of arc:";
  cin >> minutes;
  cout << "Finally, enter the  seconds of arc:";
  cin >> seconds;
  res = degrees + minutes / 60 + seconds / 360;
  cout << degrees << " degrees, " << minutes << " minutes, " << seconds
       << " seconds =  ";
  cout << res << " degrees";

  return 0;
}
