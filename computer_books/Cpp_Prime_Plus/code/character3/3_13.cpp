#include <cstring>
#include <iostream>
#include <vector>

using namespace std;

int main() {
  cout.setf(ios_base::fixed, ios_base::floatfield);
  float tree = 3;
  double debt = 7.2E12;
  cout << "tree = " << tree << endl;
  cout << "debt = " << debt << endl;
  return 0;
}
