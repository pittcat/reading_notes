#include <cstring>
#include <iostream>
#include <vector>

using namespace std;

int main() {
  long long n1, n2;
  double population;
  cout << "Enter the world's population:";
  cin >> n1;
  cout << "Enter the population of the US:";
  cin >> n2;
  population = (double)n1 / n2;
  cout << "The population of The US is" << population
       << "% of the world population.";

  return 0;
}
