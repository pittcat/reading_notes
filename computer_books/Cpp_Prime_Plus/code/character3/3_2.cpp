#include <climits>
#include <cstring>
#include <iostream>
#include <vector>

#define ZERO 0
using namespace std;

int main() {
  short sam = SHRT_MAX;
  unsigned short sue = sam;
  cout << "Sam has " << sam << " dollars and Sur has " << sue;
  cout << " dollars deposited." << endl;
  sam += 1;
  sue += 1;
  cout << "Sam has " << sam << " dollars and Sue has " << sue;
  cout << " dollars deposited.\nPoor Sam!" << endl;
  sam = ZERO;
  sue = ZERO;
  cout << "Sam has " << sam << " dollars and Sue has " << sue;
  cout << " dollars deposited.\nPoor Sam!" << endl;
  return 0;
}
