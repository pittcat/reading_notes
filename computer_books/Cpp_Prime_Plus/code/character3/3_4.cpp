#include <ios>
#include <iostream>
#include <vector>
#include <cstring>

using namespace std;

int main() {
    int chest=42,waist=42,inseam=42;
    cout<<"Monsieur cuts a striking figure!"<<endl;
    cout<<"chest = "<<chest<<" (decimal for 42)"<<endl;
    cout<<hex;
    cout<<"waist = "<<waist<<" (hexadecimal for 42)"<<endl;
    cout<<oct;
    cout<<"inseam = "<<inseam<<" (octal for 42)"<<endl;
    
    return 0;
}
