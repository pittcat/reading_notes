```cpp
class Stonewt {
private:
  enum { Lbs_per_stn = 14 };
  int stone;
  double pds_left;
  double pounds;

public:
  Stonewt(double lbs);
  Stonewt(int stn, double lbs);
  Stonewt();
  ~Stonewt();
  void show_lbs() const;
  void show_stn() const;
  // conversion functions
  operator int();
  operator double();
};
```

#### 1

```cpp
Stonewt operator*(double mult);
Stonewt Stonewt::operator*(double mult) { return Stonewt(mult * pounds); }
```

#### 2

成员函数是类定义的一部分,通过特定的对象来调用。成员函数可以隐式访问调用对象的成员,而无需使用成员运算符。友元函数不是类的组成部分,因此被称为直接函数调用。友元函数不能隐式访问类成员,而必须将成员运算符用于作为参数传递的对象。请比较复习题 1 和复习题 4 的答案。

#### 3

要访问私有成员,它必须是友元,但要访问公有成员,可以不是友元。

#### 4

```cpp
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Stonewt {
private:
  enum { Lbs_per_stn = 14 };
  int stone;
  double pds_left;
  double pounds;

public:
  Stonewt(double lbs);
  Stonewt(int stn, double lbs);
  Stonewt();
  ~Stonewt();
  Stonewt operator*(double mult);
  friend Stonewt operator*(double mult, const Stonewt &s);
  void show_lbs() const;
  void show_stn() const;
  // conversion functions
};

Stonewt Stonewt::operator*(double mult) { return Stonewt(mult * pounds); }

Stonewt operator*(double mult, const Stonewt &s) {
  return Stonewt(mult * s.pounds);
}

int main() { return 0; }
```

#### 5

```cpp
sizeof。
.。
.*。
::。
?:。
```

#### 6

这些运算符必须使用成员函数来定义。

#### 7

```
operator double()
{
  return mag;
}
```
