#include <cstring>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Cow {
  char name[20];
  char *hobby;
  double weight;

public:
  Cow();
  Cow(const char *nm, const char *ho, double wt);
  Cow(const Cow &c);
  ~Cow();
  Cow &operator=(const Cow &c);
  void ShowCow() const;
};

Cow::Cow() {
  name[0] = '\0';
  hobby = nullptr;
  weight = 0.0;
}

Cow::Cow(const char *nm, const char *ho, double wt) {
  strcpy(name, nm);
  int len;
  len = strlen(ho) + 1;
  hobby = new char[len];
  strcpy(hobby, ho);
  weight = wt;
}

Cow::Cow(const Cow &c) {

  strcpy(name, c.name);
  int len = strlen(c.hobby) + 1;
  hobby = new char[len];
  strcpy(hobby, c.hobby);
  weight = c.weight;
  cout << "copy fun called" << endl;
}

Cow &Cow::operator=(const Cow &c) {
  if (this == &c) {
    return *this;
  }
  delete[] hobby;
  strcpy(name, c.name);
  int len = strlen(c.hobby) + 1;
  hobby = new char[len];
  strcpy(hobby, c.hobby);
  weight = c.weight;
  cout << "operator fun called" << endl;
  return *this;
}
Cow::~Cow() {

  cout << name << " deleted" << endl;
  delete[] hobby;
}

void Cow::ShowCow() const {
  cout << "name:" << name << endl;
  cout << "hobby:" << hobby << endl;
  cout << "weight:" << weight << endl;
}
int main() {
  Cow a{"catr", "music", 12};
  a.ShowCow();
  Cow c = a;
  Cow b;
  b = c;
  c.ShowCow();
  return 0;
}
