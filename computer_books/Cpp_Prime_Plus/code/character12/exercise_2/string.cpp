#include "string.h"
#include <cstring>

int String::number_strings = 0;

int String::HowMany() { return number_strings; }

String::String(const char *s) {
  len = std::strlen(s);
  str = new char[len + 1];
  std::strcpy(str, s);
  number_strings++;
}

String::String(const String &s) {
  len = s.len;
  str = new char[len + 1];
  strcpy(str, s.str);
}

String::String() {
  len = 4;
  str = nullptr;
  number_strings++;
}

String::~String() {
  --number_strings;
  delete[] str;
}

String &String::operator=(const String &st) {
  if (this == &st) {
    return *this;
  }
  delete[] str;
  len = st.len;
  str = new char[len + 1];
  std::strcpy(str, st.str);
  return *this;
}

String &String::operator=(const char *s) {
  delete[] str;
  len = std::strlen(s);
  str = new char[len + 1];
  std::strcpy(str, s);
  return *this;
}

char &String::operator[](int i) { return str[i]; }

const char &String::operator[](int i) const { return str[i]; }

bool operator<(const String &st1, const String &st2) {
  return std::strcmp(st1.str, st2.str) < 0;
}

bool operator>(const String &st1, const String &st2) { return st2 < st1; }

bool operator==(const String &st1, const String &st2) {
  return std::strcmp(st1.str, st2.str) == 0;
}

ostream &operator<<(ostream &os, const String &st) {

  os << st.str;
  return os;
}

istream &operator>>(istream &is, String &st) {
  char temp[String::CINLIM];
  is.get(temp, String::CINLIM);
  if (is) {
    st = temp;
  }
  while (is && is.get() != '\n') {
    continue;
  }
  return is;
}

String operator+(const String &s1, const String &s2) {
  String res;
  res.len = s1.len + s2.len;
  res.str = new char[res.len + 1];
  strcpy(res.str, s1.str);
  strcat(res.str, s2.str);
  return res;
}

String operator+(const char *c, const String &s2) {
  String res;
  res.len = strlen(c) + s2.len;
  res.str = new char[res.len + 1];
  strcpy(res.str, s2.str);
  return res;
}

void String::Stringlow() {
  for (int i = 0; i < len; ++i) {
    str[i] = (char)tolower(str[i]);
  }
}

void String::Stringup() {
  for (int i = 0; i < len + 1; ++i) {
    str[i] = (char)toupper(str[i]);
  }
}

int String::char_count(char c) {
  int count = 0;
  for (int i = 0; i < len + 1; ++i) {
    if (c == str[i]) {
      count++;
    }
  }
  return count;
}

