#ifndef STRING_H
#define STRING_H
#include <iostream>

using namespace ::std;

class String {
private:
  char *str;
  int len;
  static int number_strings;
  static const int CINLIM = 80;

public:
  String();
  String(const char *s);
  String(const String &s);
  ~String();
  friend String operator+(const String &s1, const String &s2);
  friend String operator+(const char *c, const String &s2);
  void Stringlow();
  void Stringup();
  int char_count(char c);

  // overloaded operator methods
  int length() const { return len; };
  String &operator=(const String &);
  String &operator=(const char *);
  char &operator[](int i);
  const char &operator[](int i) const;
  // overloaded operator friends
  friend bool operator<(const String &st, const String &st2);
  friend bool operator>(const String &st, const String &st2);
  friend bool operator==(const String &st, const String &st2);
  friend ostream &operator<<(ostream &os, const String &st);
  friend istream &operator>>(istream &is, String &st);
  // static function
  static int HowMany();
};
#endif /* STRING_H */
