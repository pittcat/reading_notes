#include "stringbad.h"
#include <cstring>
#include <iostream>

using namespace std;

int StringBad::number_strings = 0;

StringBad::StringBad(const char *s) {

  len = strlen(s);
  str = new char[len + 1];
  strcpy(str, s);
  number_strings++;
  cout << number_strings << ": \"" << str << "\" object created\n";
}

StringBad::StringBad() {
  len = 4;
  str = new char[4];
  strcpy(str, "C++");
  number_strings++;
  cout << number_strings << ": \"" << str << "\" default object created\n";
};

StringBad::~StringBad() {
  cout << "\"" << str << "\" object deleted, ";
  --number_strings;
  cout << number_strings << " left \n";
  delete[] str;
}

StringBad::StringBad(const StringBad &st) {
  number_strings++;
  len = st.len;
  str = new char[len + 1];
  strcpy(str, st.str);
  cout << number_strings << ":\"" << str << "\" object created\n";
}

ostream &operator<<(ostream &os, const StringBad &st) {

  os << st.str;
  return os;
}

StringBad &StringBad::operator=(const StringBad &) {

}
