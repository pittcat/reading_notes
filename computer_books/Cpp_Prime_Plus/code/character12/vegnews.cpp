#include "stringbad.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

void callme1(StringBad &);
void callme2(StringBad);

int main() {

  cout << "Starting an inner book.\n";
  StringBad headline1("Celery Stalks at Midnight");
  StringBad headline2("Lettuce Prey");
  StringBad sports("Spinach Leaves Bowl for Dollar");
  cout << "headline1: " << headline1 << endl;
  cout << "headline2: " << headline2 << endl;
  cout << "sports: " << sports << endl;
  callme1(headline1);
  cout << "headline1: " << headline1 << endl;
  callme2(headline2);
  cout << "headline2: " << headline2 << endl;
  cout << "Initalize one object to another:\n";
  StringBad sailor = sports;
  cout << "sailor: " << sailor << endl;
  cout << "Assign one object to another:\n";
  StringBad knot;
  knot = headline1;
  cout << "knot: " << knot << endl;
  cout << "Exiting the block.\n";

  return 0;
}

void callme1(StringBad &rsb) {
  cout << "String passed by reference:\n";
  cout << " \"" << rsb << endl;
}

void callme2(StringBad sb) {
  cout << "String passed by value:\n";
  cout << " \"" << sb << "\"\n";
}
