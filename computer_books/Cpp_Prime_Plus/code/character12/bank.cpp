#include "queue.h"
#include <cstdlib>
#include <ctime>
#include <iostream>

using namespace ::std;

const int MIN_PER_HR = 60;

bool newcustomer(double x);

int main(int argc, char *argv[]) {
  srand(time(0));
  cout << "Case Study: Bank of Heather Automatic Teller\n";
  cout << "Enter maximum size of queue:";
  int qs;
  cin >> qs;
  Queue line(qs); // line queue holds up to qs people

  cout << "Enter the number of simulation hours: ";
  int hours;
  cin >> hours;
  long cyclelimit = MIN_PER_HR * hours;

  cout << "Enter the average number of customers per hour:";
  double perhour;
  cin >> perhour;

  double min_per_cust;
  min_per_cust = MIN_PER_HR;

  Item temp;
  long turnaways = 0;
  long customers = 0;
  long serverd = 0;
  long sum_line = 0;
  int wait_time = 0;
  long line_wait = 0;

  for (int cycle = 0; cycle < cyclelimit; ++cycle) {
    if (newcustomer(min_per_cust)) {

      if (line.isfull()) {
        turnaways++;
      } else {
        customers++;
        temp.set(cycle);
        line.enqueue(temp);
      }
    }

    if (wait_time <= 0 && !line.isempty()) {
      line.dequeue(temp);
      wait_time = temp.ptime();
      line_wait += cycle - temp.when();
      serverd++;
    }
    if (wait_time > 0) {
      wait_time--;
    }
    sum_line += line.queuecount();
  }

  return 0;
}

bool newcustomer(double x) { return rand() * x / RAND_MAX < 1; }
