#### 1

```cpp
class String{
  private:
    char *str;
    int len;
}
```

a．语法是正确的，但该构造函数没有将 str 指针初始化。该构造函数应将指针设置成 NULL 或使用 new [ ] 来初始化它。

b. 该构造函数没有创建新的字符串，而只是复制了原有字符串的地址。它应当使用 new [ ] 和 strcpy( )。

```cpp
String::String(const char *s){
  len = strlen(s);
  str = new char[len+1];
  strcpy(str,s);
}
```

c．它复制了字符串，但没有给它分配存储空间，应使用 new char[len+ 1] 来分配适当数量的内存。

#### 2

首先，当这种类型的对象过期时，对象的成员指针指向的数据仍将保留在内存中，这将占用空间，同时不可访问，因为指针已经丢失。可以让类析构函数删除构造函数中 new 分配的内存，来解决这种问题

其次，析构函数释放这种内存后，如果程序将这样的对象初始化为另一个对象，则析构函数将试图释放这些内存两次。这是因为将一个对象初始化为另一个对象的默认初始化，将复制指针值，但不复制指向的数据，这将使两个指针指向相同的数据。解决方法是，定义一个复制构造函数，使初始化复制指向的数据。第三，将一个对象赋给另一个对象也将导致两个指针指向相同的数据。解决方法是重载赋值运算符，使之复制数据，而不是指针。

#### 3
C++自动提供下面的成员函数：如果没有定义构造函数，将提供默认构造函数。如果没有定义复制构造函数，将提供复制构造函数。如果没有定义赋值运算符，将提供赋值运算符。如果没有定义析构函数，将提供默认析构函数。如果没有定义地址运算符，将提供地址运算符。


#### 4
```cpp
class nifty {
private:
  char personality[40];
  int talents;

public:
  nifty();
  nifty(const char *s);
  friend ostream &operator<<(ostream &os, const nifty &n);
};

nifty::nifty() {
  personality[0] = '\0';
  talents = 0;
}

nifty::nifty(const char *s) {
  strcpy(personality, s);
  talents = 0;
}

ostream &operator<<(ostream &os, const nifty &n) {
  os << n.personality << '\n';
  os << n.talents << '\n';
  return os;
}

```
