#ifndef _STINGBAD_H_
#define _STINGBAD_H_

#include <iostream>
#include <ostream>

class StringBad {
private:
  char *str;
  int len;
  static int number_strings;

public:
  StringBad(const char *s);
  StringBad(const StringBad &);
  StringBad();
  ~StringBad();
  friend std::ostream &operator<<(std::ostream &os, const StringBad &st);
};

#endif /* !STINGBAD_H */
