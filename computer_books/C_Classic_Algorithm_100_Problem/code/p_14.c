#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int isPrime(int num);
int main() {
  int n, i;
  printf("Please input a number:");
  scanf("%d", &n);
  if (isPrime(n)) {
    printf("%d = %d", n, n);
    return 0;
  }
  printf("%d = ", n);
  do {
    for (int i = 2; i < 10; i++) {
      if (n % i == 0 && isPrime(i)) {
        printf("%d*", i);
        n /= i;
        if (isPrime(n)) {
          printf("%d", n);
          break;
        }
      }
    }
  } while (!isPrime(n));
  return 0;
}

int isPrime(int num) {
  if (num == 2) {
    return 1;
  } else if (num % 2 == 0) {
    return 0;
  }
  for (int i = 3; i <= (int)sqrt((double)num); i += 2) {
    if (num % i == 0) {
      return 0;
    }
  }
  return 1;
}
