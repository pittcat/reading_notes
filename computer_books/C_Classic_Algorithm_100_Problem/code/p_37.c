#include <stdio.h>
#include <stdlib.h>

int array[10];

int main() {
  printf("Please input ten numbers:");
  int t;
  for (int i = 0; i < 10; i++) {
    scanf("%d", &array[i]);
  }
  for (int j = 0; j < 10; j++) {
    for (int l = j + 1; l < 10; l++) {
      if (array[j] > array[l]) {
        t = array[j];
        array[j] = array[l];
        array[l] = t;
      }
    }
  }
  for (int c = 0; c < 10; ++c) {
    printf("%d ", array[c]);
  }
  return 0;
}
