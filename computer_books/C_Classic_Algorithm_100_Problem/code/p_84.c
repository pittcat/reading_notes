#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int isPrime(int num);

int main() {
  int num, a, b;
  printf("Please input an even number:");
  /* scanf("%d", &num); */
  num = 6;
  a = 2;
  while (a != num) {
    b = num - a;
    if (isPrime(a) && isPrime(b)) {
      break;
    }
    a++;
  }
  printf("%d = %d + %d", num, a, b);

  return 0;
}

int isPrime(int num) {
  if (num == 2) {
    return 1;
  } else if (num % 2 == 0) {
    return 0;
  }
  for (int i = 3; i <= (int)sqrt((double)num); i += 2) {
    if (num % i == 0) {
      return 0;
    }
  }
  return 1;
}
