#include <stdio.h>
#include <stdlib.h>

#define N 5
int main() {
  int a[N] = {9, 6, 5, 4, 1}, i, j;
  printf("orderd:");
  for (i = 0; i < 5; ++i) {
    printf("%d ", a[i]);
  }
  printf("\n");
  printf("reversed:");

  for (j = 4; j > -1; --j) {
    printf("%d ", a[j]);
  }
  return 0;
}
