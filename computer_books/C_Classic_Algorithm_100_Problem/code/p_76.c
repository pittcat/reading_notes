#include <stdio.h>
#include <stdlib.h>

int main() {
  int n, i;
  float t, sum = 0.0;
  printf("Please input number:");
  scanf("%d", &n);
  if (n % 2 == 0) {
    for (i = 2; i <= n; i += 2) {
      sum += 1 / i;
    }
  } else {
    for (int i = 1; i <= n; i += 2) {
      sum += 1 / i;
    }
  }
  printf("sum = %f", sum);
  return 0;
}
