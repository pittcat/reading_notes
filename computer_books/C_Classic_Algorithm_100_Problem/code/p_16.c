#include <stdio.h>
#include <stdlib.h>

int main() {
  int m, n, t = 1, product;
  printf("Please input num m and num n:");
  scanf("%d %d", &m, &n);
  product = m * n;
  if (m < n) {
    t = m;
    m = n;
    n = t;
  }
  while (t != 0) {
    t = m % n;
    m = n;
    n = t;
  }
  printf("gcd is:%d\n", m);
  printf("lcm is %d\n", product/m);
  return 0;
}
