#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int isPrime(int num);
int main() {
    printf("Prime number:");
    for (int i = 0; i < 100; i++) {
      if (isPrime(i)) {
        printf("%d\n",i);
      }
    }
    return 0;
}

int isPrime(int num) {
  if (num == 2) {
    return 1;
  } else if (num % 2 == 0) {
    return 0;
  }
  for (int i = 3; i <= (int)sqrt((double)num); i += 2) {
    if (num % i == 0) {
      return 0;
    }
  }
  return 1;
}
