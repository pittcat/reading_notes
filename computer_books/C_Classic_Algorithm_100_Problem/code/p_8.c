#include <stdio.h>
#include <stdlib.h>

int main() {
  int i, j, res;
  for (int i = 1; i < 10; i++) {
    for (int j = 1; j < 10; j++) {
      res = i * j;
      printf("%d * %d = %d ", i, j, res);
    }
    printf("\n");
  }
  return 0;
}
