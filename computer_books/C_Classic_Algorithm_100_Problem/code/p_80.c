#include <stdio.h>
#include <stdlib.h>

int main() {
  int count, i, j, cur, alloc, left;
  for (i = 5; i < 100000; ++i) {
    cur = i;
    for (j = 0; j < 4; ++j) {
      if ((cur - 1) % 5 == 0) {
        alloc = (cur - 1) / 5;
        cur -= alloc;
      } else {
        break;
      }
    }
    if (j == 3) {
      printf("count is %d", i);
      break;
    }
  }
  return 0;
}
