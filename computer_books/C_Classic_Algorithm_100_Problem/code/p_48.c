#include <stdio.h>
#include <stdlib.h>
#define LAG >
#define SMA <
#define EQ ==

int main() {
  int i = 10, j = 20;
  if (i LAG j) {
    printf("%d larger than %d\n", i, j);
  } else if (i EQ j) {
    printf("%d equal to %d\n", i, j);
  } else if (i SMA j) {
    printf("%d smaller than %d", i, j);
  } else {
    printf("No such value\n");
  }
  return 0;
}
