#include "p_50.h"
#include <stdio.h>
#include <stdlib.h>

int main() {
  int i = 10;
  int j = 20;
  if (i LAG j)
    printf("\40: %d larger than %d \n", i, j);
  else if (i EQ j)
    printf("\40: %d equal to %d \n", i, j);
  else if (i SMA j)
    printf("\40:%d smaller than %d \n", i, j);
  else
    printf("\40: No such value.\n");
}
