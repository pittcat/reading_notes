#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

int main() {
  int len, num = 0;
  char s[10];
  fgets(s, 10, stdin);
  len = strlen(s)-1;
  for (int i = 0; i < len; ++i) {
    num += (int)(s[i] - '0') * (int)pow(8.0, (double)len - i - 1);
  }
  printf("%d", num);
  return 0;
}
