#include <stdio.h>
#include <stdlib.h>

#define N 4
static struct man {
  char name[20];
  int age;
} person[N] = {"li", 18, "wang", 19, "zhang", 40, "sun", 22};
int main() {
  struct man *q, *p;
  int i;
  p = person, q = person;
  for (i = 0; i < 5; ++i) {
    if (q->age > p->age) {
      p = q;
    }
    q++;
  }
  printf("%s %d", p->name, p->age);
}
