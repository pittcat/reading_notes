#include <stdio.h>
#include <stdlib.h>

int main() {
  long int r1 = 1, r2 = 1, t;
  printf("%ld\n%ld\n", r1, r2);
  for (int i = 0; i < 20; ++i) {
    t = r2;
    r2 = r1 + r2;
    r1 = t;
    printf("%d\n", r2);
  }
  return 0;
}
