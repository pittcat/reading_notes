#include <stdio.h>
#include <stdlib.h>

void swap(int *a, int *b);

int main() {
  int n1 = 3, n2 = 1, n3 = 2;
  int *p1, *p2, *p3;
  p1 = &n1, p2 = &n2, p3 = &n3;
  if (n1 > n2) {
    swap(p1, p2);
  }
  if (n1 > n3) {
    swap(p1, p3);
  }
  if (n2 > n3) {
    swap(p2, p3);
  }
  printf("n1 n2 n3:%d %d %d", n1, n2, n3);
  return 0;
}

void swap(int *a, int *b) {
  int tpm = *a;
  *a = *b;
  *b = tpm;
}
