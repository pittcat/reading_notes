#include <stdio.h>
#include <stdlib.h>

int main() {
  int a, n, sum = 0, t;

  scanf("%d%d", &a, &n);
  printf("a=%d n=%d", a, n);
  t = a;
  for (int i = 0; i < n; i++) {
    sum += t;
    t = t * 10 + a;
  }
  printf("a+aa+aaa+aaaa+aa...=%d", sum);
  return 0;
}
