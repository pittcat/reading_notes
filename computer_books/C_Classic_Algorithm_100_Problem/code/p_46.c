#include <stdio.h>
#define TRUE 1
#define FALSE 0
#define SQ(x) x *x

int main(void) {
  int num, again;
  printf("\40:Program will stop if input value less than 50\n");
  while (again) {
    printf("\40:Please input a number===>");
    scanf("%d", &num);
    printf("\40:The square for this number is %d\n", SQ(num));
    if (num >= 50) {
      again = TRUE;
    } else {
      again = FALSE;
    }
  }
  return 0;
}
