#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  int i;
  double x, y;
  for (i = 0; i < 100000; ++i) {
    x = (int)sqrt((double)i + 100);
    y = (int)sqrt((double)i + 268);
    if (x * x == i + 100 && y * y == i + 268) {
      printf("%d\n", i);
    }
  }
  return 0;
}
