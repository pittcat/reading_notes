#include <stdio.h>
#include <stdlib.h>

int main() {
    int t_num,sum=0;
    for (int i = 1; i < 21; i++) {
      t_num=1;
      for (int j = 1; j < i+1; j++) {
        t_num*=j;
      }
      sum+=t_num;
    }
    printf("1!+2!+3!+... = %d",sum);
    return 0;
}
