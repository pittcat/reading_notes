#include <stdio.h>
#include <stdlib.h>

int main() {
  int count = 0, n;
  scanf("%d", &n);
  printf("reverse numbers:");
  for (int i = 0; i < 5; ++i) {
    if (n) {
      count++;
      printf("%d", n % 10);
      n /= 10;
    } else {
      break;
    }
  }

  printf("\n%d wei number", count);
  return 0;
}
