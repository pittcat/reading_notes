#include <stdio.h>
#include <stdlib.h>

int max(int *array, int size);
int min(int *array, int size);
void swap(int *a, int *b);

int main() {
  int numbers[10], max_v, min_v;
  printf("Please input number:");
  for (int i = 0; i < 10; i++) {
    scanf("%d", &numbers[i]);
  }
  swap(&numbers[0], &numbers[max(numbers, 10)]);
  swap(&numbers[0], &numbers[min(numbers, 10)]);
    printf("swap res:");
  for (int i = 0; i < 10; i++) {
    printf("%d ",numbers[i]);
  }
  return 0;
}

int max(int *array, int size) {
  int max_value_idx = 0;
  for (int i = 0; i < size; i++) {
    if (max_value_idx > array[i]) {
      max_value_idx = i;
    }
  }

  return max_value_idx;
}

int min(int *array, int size) {
  int min_value_idx = 0;
  static int res[2];
  for (int i = 0; i < size; i++) {
    if (min_value_idx < array[i]) {
      min_value_idx = i;
    }
  }
  return min_value_idx;
}

void swap(int *a, int *b) {
  int tpm = *a;
  *a = *b;
  *b = tpm;
}
