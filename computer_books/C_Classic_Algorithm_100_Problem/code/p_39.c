#include <stdio.h>
#include <stdlib.h>

int main() {
  int a[11] = {1, 4, 6, 9, 13, 16, 19, 28, 40, 100}, num, idx;
  printf("Please input insert number:");
  scanf("%d", &num);
  if (a[9] < num) {
    a[10] = num;
  } else {

    for (int i = 0; i < 10; i++) {
      if (a[i] > num) {
        idx = i;
        break;
      }
    }
    for (int j = 10; j > idx; j--) {
      a[j] = a[j - 1];
    }
    a[idx] = num;
  }
  for (int i = 0; i < 11; i++) {
    printf("%d ", a[i]);
  }
  return 0;
}
