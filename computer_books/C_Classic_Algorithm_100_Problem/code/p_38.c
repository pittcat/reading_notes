#include <stdio.h>
#include <stdlib.h>

int main() {
  float array[3][3], sum;
  printf("please input rectangle elements:");

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      scanf("%f", &array[i][j]);
    }
  }
  for (int k = 0; k < 3; k++) {
    sum += array[k][k];
  }
  printf("sum:%f", sum);
  return 0;
}
