#include <stdio.h>

int main() {
  int i, j;
  int a[10][10], count = 1;
  for (i = 0; i < 10; i++) {
    a[i][0] = 1;
    a[i][i] = 1;
  }
  for (int i = 2; i < 10; ++i) {
    for (int j = 1; j < i; ++j) {
      a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
    }
  }
  for (int idx_r = 0; idx_r < 10; ++idx_r) {
    for (int idx_c = 0; idx_c < count; ++idx_c) {
      printf("%d ", a[idx_r][idx_c]);
    }
    count++;
    printf("\n");
  }
}
