#include <stdio.h>
#include <stdlib.h>

int main() {
  int n, m, *arr1 = (int *)malloc(n * sizeof(int)),
            *arr2 = (int *)malloc(m * sizeof(m));
  n = 5, m = 2;
  for (int i = 0; i < n; i++) {
    scanf("%d", &arr1[i]);
  }
  for (int j = 0; j < m; j++) {
    arr2[j] = arr1[j];
  }
  for (int k = 0; k < n - m; k++) {
    arr1[k] = arr1[k + m];
  }
  for (int l = 0; l < m; l++) {
    arr1[l + m + 1] = arr2[l];
  }
  for (int i = 0; i < n; ++i) {
    printf("%d ", arr1[i]);
  }
  return 0;
}
