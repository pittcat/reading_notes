#include <stdio.h>
#include <stdlib.h>

int main() {
  int i, n = 0, t;
  while (n < 7) {
    scanf("%d", &t);
    if (t > 0 && t < 51) {
      for (i = 0; i < t; ++i) {
        printf("*");
      }
      n++;
      printf("\n");
    }
  }
  return 0;
}
