#include <stdio.h>
#include <stdlib.h>

int fact(unsigned int n);

int main() {
  int i = 5;
  printf("5!=%d", fact(i));
  return 0;
}

int fact(unsigned int n) {
  if (n <= 1) {
    return n;
  } else {
    return n * fact(n - 1);
  }
}
