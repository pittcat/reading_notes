#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

int main() {
  register int i;
  int tmp = 0;
  for (i = 1; i <= 100; i++)
    tmp += i;
  printf("The sum is %d\n", tmp);
}
/* 0.00s user 0.00s system 81% cpu 0.001 total */
