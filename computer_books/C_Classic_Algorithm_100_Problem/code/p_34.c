#include <stdio.h>
#include <stdlib.h>

void hello(void);
void threehello(void);
int main() {
    threehello();
    return 0;
}


void hello(void)
{
  printf("Hello\n");
}

void threehello(void)

{
  int i;
  for (i = 0; i < 3; ++i) {
    hello();
  }
}
