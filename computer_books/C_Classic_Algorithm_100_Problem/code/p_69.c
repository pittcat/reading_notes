#include <stdio.h>
#include <stdlib.h>

int main() {
  int num, *array, count = 0, idx, flag = 0, rest;
  printf("Please input the number of people:\n");
  /* scanf("%d", &num); */
  num = 110;
  array = (int *)malloc(num * sizeof(int));
  for (int i = 0; i < num; i++) {
    array[i] = 1;
  }
  while (count != num - 1) {
    idx = 1;
    for (int j = 0; j < num; j++) {
      if (array[j] != 0) {
        if (idx % 3 == 0) {
          array[j] = 0;
          count++;
        }
        idx++;
      }
    }
    if (num - count) {
      break;
    }
  }
  rest = num - count;
  for (int i = 0; i < num; ++i) {
    if (rest == 1 && array[i]) {
      printf("%d", i);
    } else if (array[i] == 1) {
      rest--;
    }
  }
  free(array);
  return 0;
}
