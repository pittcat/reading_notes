#include <stdio.h>
#include <stdlib.h>

int main() {
  int t;
  double sum = 0.0, numerator = 2.0, denominator = 1.0;
  for (int i = 1; i < 20; i++) {
    sum += (float)numerator / denominator;
    t = numerator;
    numerator = numerator + denominator;
    denominator = t;
  }
  printf("%9.6f", sum);
  return 0;
}
