#include <stdio.h>
#include <stdlib.h>

int main() {
  float state = 100.0, sum = 100;
  for (int i = 1; i < 10; i++) {
    state /= 2;
    sum += state * 2;
  }
  printf("sum:%f\n", sum);
  printf("last hight:%f", state / 2);
  return 0;
}
