#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int isPrime(int num);

int main() {
  int flag = 0;
  for (int i = 100; i < 201; i++) {
    if (isPrime(i)) {
      printf("%d", i);
    }
  }
  return 0;
}

int isPrime(int num) {
  if (num < 3) {
    return num == 2;
  }
  for (int i = 2; i <= (int)sqrt((double)num); i += 2) {
    if (num % i == 0) {
      return 0;
    }
  }
  return 1;
}
