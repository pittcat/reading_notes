from itertools import permutations
items = ['a', 'b', 'c']
for p in permutations(items):
    print(p)

for p in permutations(items, 2):
    print(p)
