#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import namedtuple

Subscriber = namedtuple('Subscriber', ['addr', 'joined'])
sub = Subscriber('jonesy@example.com', '2012-10-19')

print(sub)
print(len(sub))
