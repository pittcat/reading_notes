#!/usr/bin/env python
# -*- coding: utf-8 -*-

def decorator(f):
    def new_function():
        print("Extra Functionality")
        f()
    return new_function


@decorator
def initial_funcion():
    print("Initial fucntionality")
        
initial_funcion()
