def avg(first, *rest):
    return (first + sum(rest)) / (1 + len(rest))


print(avg(1, 3))
print(avg(1, 3, 4, 5))


def a(x, *args, y):
    pass


def b(x, *args, y, **kwargs):
    pass
