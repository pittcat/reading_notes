#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict

d = {'a': [1, 2, 3], 'b': [4, 5]}
e = {'a': {1, 2, 3}, 'b': {4, 5}}

d = defaultdict(list)

d['a'].append(1)
d['a'].append(2)
d['b'].append(4)

print(d)

d = defaultdict(set)

d['a'].add(1)
d['a'].add(2)
d['b'].add(4)
print(d)
d = {}
d.setdefault('a', []).append(1)
d.setdefault('a', []).append(2)
print(d)

d = {}

pair = {}
for key, value in pair:
    if key not in pair:
        pair[key] = []
    pair[key].append(value)

pair = defaultdict(list)

for key, value in pair:
    pair[key].append(value)
