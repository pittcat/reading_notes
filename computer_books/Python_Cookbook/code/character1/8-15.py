#!/usr/bin/env python
# -*- coding: utf-8 -*-


class A:
    """description"""
    def spam(self, x):
        pass

    def foo(self, ):
        pass


class B1:
    """
    """
    def __init__(self):
        self._a = A()

    def spam(self, x):
        return self._a.spam(x)
    def foo(self):
        return self._a.foo()
    def bar(self):
        pass


