#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math


class lazeproperty:
    """
    """
    def __init__(self, func):
        self.func = func

    def __get__(self, instance, cls):
        if instance is None:
            return self
        else:
            value = self.func
            setattr(instance, self.func.__name__, value)
            #  setattr(x, 'foobar', 123) ==> x.foobar = 123
            return value


class Circle:
    """
    """
    def __init__(self, radius):
        self.radius = radius

    @lazeproperty
    def area(self):
        print('Computing area')
        return math.pi * self.radius**2

    @lazeproperty
    def perimeter(self):
        print('Computing perimeter')
        return 2 * math.pi * self.radius


c = Circle(4.0)

print(c.radius)
print(c.area)
print(c.area)
print(c.perimeter)
