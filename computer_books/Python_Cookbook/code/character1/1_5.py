#!/usr/bin/env python
# -*- coding: utf-8 -*-

import heapq


class PriorityQueue:
    """description"""
    def __init__(self) -> None:
        self._queue = []
        self._index = 0

    def push(self, item, priority):
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        return heapq.heappop(self._queue)[-1]


class Item:
    """
    """
    def __init__(self,name) -> None:
        self.name = name

    def __repr__(self) -> str:
        return 'Item{!r}'.format(self.name)

