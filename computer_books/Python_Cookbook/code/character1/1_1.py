#!/usr/bin/env python
# -*- coding: utf-8 -*-

s = "Hello"
a, b, c, d, e = s
print(a)

data = ["ACME", 20, 91.1, (2012, 12, 21)]
_, shares, price, _ = data
print(shares)
print(price)
