class Node(object):
    """docstring for Node"""
    def __init__(self, value):

        self._value = value
        self._children = []

    def __repr__(self) -> str:
        return 'Node({!r})'.format(self._value)

    def add_child(self, node):
        self._children.append(node)

    def __iter__(self):
        return iter(self._children)

    def depth_first(self):
        yield self
        for c in self:
            yield from c.depth_first()
