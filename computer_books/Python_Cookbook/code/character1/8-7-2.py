#!/usr/bin/env python
# -*- coding: utf-8 -*-


class A:
    """
    """
    def __init__(self):
        self.x = 0


class B(A):
    """description"""
    def __init__(self):
        super().__init__()
        self.y = 1
