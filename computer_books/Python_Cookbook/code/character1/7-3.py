#!/usr/bin/env python
# -*- coding: utf-8 -*-


def add(x: int, y: int) -> int:
    return x + y
