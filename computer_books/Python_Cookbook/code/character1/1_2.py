#!/usr/bin/env python
# -*- coding: utf-8 -*-

record = ('Dave', 'dave@example.com', '773-555-1212', '847-555-1212')

name, email, *phone_numbers = record  # phone_numbers always list type

print(name)
print(email)

print(phone_numbers)

records = [
    ('foo', 1, 2),
    ('bar', 'hello'),
    ('foo', 3, 4),
]


def do_foo(x, y):
    """TODO: Docstring for do_foo.

    :function: TODO
    :returns: TODO

    """
    print('foo', x, y)


def do_bar(s):
    """TODO: Docstring for do_bar.

    :function: TODO
    :returns: TODO

    """
    print('bar', s)


for tag, *args in records:
    if tag == 'foo':
        do_foo(*args)
    elif tag == 'bar':
        do_bar(*args)

line = 'nobody:*:-2:-2:Unprivileged User:/var/empty:/usr/bin/false'
print(line.split(':'))
uname, *fields, homedir, sh = line.split(':')
print(uname)

record = ('ACME', 50, 123.45, (12, 18, 2012))

name, *_, (*_, year) = record
print(name)

items = [1, 10, 7, 4, 5, 9]
head, *tail = items
print(head)
print(tail)


def sum(items):
    """docstring for sum"""
    head, *tail = items
    return head + sum(tail) if tail else head
