def apply_async(func, args, *, callback):
    result = func(*args)
    callback(result)


def print_result(result):
    print("Got:", result)


def add(x, y):
    return x + y


res = apply_async(add, (2, 3), callback=print_result)
print(res)


class ResultHandler(object):
    """docstring for ResultHandler"""
    def __init__(self):
        self.sequence = 0

    def handler(self, result):
        self.sequence += 1
        print('[{}] Got: {}'.format(self.sequence, result))


r = ResultHandler()
print(apply_async(add, (2, 3), callback=r.handler))
print(apply_async(add, ('hello', 'world'), callback=r.handler))


def make_handler():
    sequence = 0
    while True:
        result = yield
        sequence += 1
        print('[{}] Got: {}'.format(sequence, result))
