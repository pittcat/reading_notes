#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
nums = [1, 2, 3, 4, 5]
s = sum(x * x for x in nums)
print(s)

files = os.listdir('.')
if any(os.name.endswith('.py') for name in files):
    print("There be python")
else:
    print("Sorry,no python")

s = ('ACME', 50, 123.45)
print(','.join(str(x) for x in s))
