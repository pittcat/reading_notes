from operator import itemgetter
from itertools import groupby
rows = [{
    'address': '5412 N CLARK',
    'date': '07/01/2012'
}, {
    'address': '5148 N CLARK',
    'date': '07/04/2012'
}, {
    'address': '5800 E 58TH',
    'date': '07/02/2012'
}, {
    'address': '2122 CLARK',
    'date': '07/03/2012'
}]
rows.sort(key=itemgetter('date'))

for date, items in groupby(rows, key=itemgetter('date')):

    print(date)
    for i in items:
        print(' ', i)
