#!/usr/bin/env python
# -*- coding: utf-8 -*-

class A:
    """
    """
    def spam(self, ):
        print('A.spam')
        
class B(A):
    """description"""
    def spam(self):
        print('B.spam')
        super().spam()
