#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Pair:
    """
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self) -> str:
        return 'Pair({0.x!r},{0.y!r})'.format(self)

    def __str__(self) -> str:
        return '({0.x!s},{0.y!s})'.format(self)


