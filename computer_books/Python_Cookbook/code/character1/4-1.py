def manual_iter():
    """docstring for manual_iter"""
    with open('./password') as f:
        try:
            while True:
                line = next(f)
                print(line, end=' ')

        except StopIteration:
            pass


#  manual_iter()

items = [1, 3, 4]

it = iter(items)
print(next(it))
print(next(it))
print(next(it))
