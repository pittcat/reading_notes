#!/usr/bin/env python
# -*- coding: utf-8 -*-

from abc import ABCMeta,abstractclassmethod

class IStream(metaclass=ABCMeta):
    """description"""
    @abstractclassmethod
    def read(self, maxbytes=1):
        pass
    @abstractclassmethod
    def write(self,data):
        pass
a=IStream()


class SocketStream(IStream):
    """description"""
    def read(self, maxbytes=-1):
        pass
    def write(self, data):
        pass




def serialize(obj,stream):
    if not isinstance(stream,IStream):
        raise TypeError('Expected an IStream')
    pass



    


