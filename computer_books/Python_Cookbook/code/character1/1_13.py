from operator import itemgetter
rows = [
    {
        'fname': 'Brian',
        'Iname': ' Jones',
        'uid': 1003
    },
    {
        'fname': 'David',
        'Iname': 'Beazley',
        'uid': 1002
    },
    {
        'fname': 'John',
        'Iname': 'Cleese',
        'uid': 1001
    },
]

row_by_fname = sorted(rows, key=itemgetter('fname'))
#  row_by_fname = sorted(rows, key=lambda r: r['fname'])
row_by_uid = sorted(rows, key=itemgetter('uid'))
print(row_by_fname)
print(row_by_uid)

