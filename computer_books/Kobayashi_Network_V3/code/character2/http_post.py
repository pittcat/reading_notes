#!/usr/bin/env python
# -*- coding=utf-8 -*-

import http.client

connection = http.client.HTTPConnection("www.journaldev.com")

connection.request("GET","/")

response = connection.getresponse()

print(response.getheaders())
